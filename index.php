<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/html" xml:lang="ru" lang="ru">
<head>
	<title>Такси Колеса - служба такси в Алматы. Вызов такси в Алматы.</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width">
    <script src="js/jquery-1.11.2.js"></script>
    <script src="js/jquery.maskedinput.js"></script>
    <script src="js/datetimepicker_css.js"></script>
    <script src="js/moment.js"></script>
    <script src="http://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script src="js/yandex_map.js" type="text/javascript"></script>
    <script src="js/script.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <link rel="stylesheet" href="css/style.css">
<body>
<img src="img/airport.png" class="bgd_right" alt="sexy taxi">
<img src="img/suit_v4_d.png" class="bgd_left" alt="sexy taxi">
<div class="overlay" title="окно"></div>
<div class="popup">
	<div class="close_window">x</div>
	<p style="    font-size: 20px;    text-align: center;">Введите ваше имя и телефон и мы с вами свяжемся!</p>
	<form action="call.php" method="post">
		<input type="text" name="call_name" placeholder="Ваше имя" id="call_name" class="call_t">
		<input type="text" name="call_phone" placeholder="Ваш телефон" id="call_phone" class="call_t">
		<input type="button" value="Жду звонка!"  id="submit_call" class="cnt_3_2_1_btn" name="send">
	</form>
</div>

<div id="loader-wrapper">
    <div id="loader"></div>
</div>
<div class="content">
	<div class="menu">
        <div class="menu_1">
			<a class="" href="#"><img class="logo" src="img/logo_taxi_kolesa.png"></a>
		</div>
		<div class="menu_2">
			<a class="menu_item" href="#cnt_zakaz">сделать заказ</a>
			<a class="menu_item" href="#cnt_tarify">тарифы</a>	
			<a class="menu_item" href="#cnt_otzyvy">отзывы</a>	
			<a class="menu_item" href="#cnt_voditelyam">водителям</a>	

			<a class="menu_item" href="#cnt_contakty">контакты</a>	
		</div>
		<div class="menu_3">
			<a class="menu_item" style="margin: 0 40px 0 0;" href="#cnt_korporat">Корпоративным клиентам</a>	<div class="menu_3_1">+7 (727) 338-77-77</div>     <div class="menu_3_1">+7 (702) 221-30-30</div>
			<img src="img/24.png" alt="24 часа" class="hour24">
		</div>
	</div>

	<div class="cnt_1">
		<div class="cnt_1_1">
			Бизнес такси по цене Эконом
		</div>
		<div>
			<div class="cnt_1_1_1">
				<a target="_blank" href="http://est4u.org/1Q6lArmj"><img class="apps" src="img/appstore.png"></a>
				<a target="_blank" href="http://est4u.org/1Q6lArmj"><img class="apps" src="img/googleplay.png"></a>
			</div>
			<div class="cnt_1_1_2">
				<a target="_blank" href="http://vk.com/id297311396"><img class="scn" src="img/vk-64.png"></a>
				<a target="_blank" href="https://www.facebook.com/profile.php?id=100009004909805&fref=ts"><img class="scn" src="img/facebook-60.png"></a>
				<a target="_blank" href="https://instagram.com/taxikolesa_almaty/"><img class="scn_l" src="img/instagram-60.png"></a>
			</div>
		</div>
	</div>

	<div class="cnt_2">
		<div class="cnt_2_1"><img src="img/passanger.png">По городу от 300 тг</div>
		<div class="cnt_2_2"><img src="img/wine.png">Трезвый водитель от 1 200 тг</div>
		<div class="cnt_2_3"><img src="img/plane.png">Аэропорт от 1 100 тг</div>
	</div>

	<div class="cnt_3" name="cnt_zakaz">
        <form id="calltaxi-form" method="post">
		<div class="cnt_3_1">
			<div class="cnt_3_1_1">
				<label>Откуда:</label>
				<input type="text" value="" placeholder="Адрес подачи" class="" name="SOURCE" id="CalltaxiForm_OrderSourceAddress">
			</div>
			<div class="cnt_3_1_1">
				<label>Куда:</label>
				<input type="text" value="" placeholder="Адрес назначения" class="" name="DEST" id="CalltaxiForm_SourceAddress">
			</div>
			<div class="cnt_3_1_1">
				<label>Имя:</label>
				<input type="text" value="" class="" placeholder="Введите ваше имя" name="PASSENGER" id="PASSENGER">
			</div>
			<div class="cnt_3_1_1">
				<label>Телефон:</label>
				<input type="text" value="" class="" placeholder="Введите ваш номер телефона" name="PHONE_TO_DIAL" id="PHONE_TO_DIAL">
			</div>
			<div class="cnt_3_1_1">
				<label>Время:</label>
				<div class="cnt_3_1_1_1">
					<div class="cnt_3_1_1_2 ">Сейчас</div>
					<div class="cnt_3_1_1_3 activ_time_t">Назначить</div>
				</div>
				<div class="sel_time" style="display: none;">
                    <div onclick="select_time(1)" id="minuty1" class="minuty" style="color: #414042; background: #feef00;"><p style="margin-top: 20px;">Сейчас</p></div>
                    <div onclick="select_time(2)" id="minuty2" class="minuty" style="color: #414042; background: #f4f4f4;"><p><span class="mmm">+15</span><br>минут</p></div>
                    <div onclick="select_time(3)" id="minuty3" class="minuty" style="color: #414042; background: #f4f4f4;"><p><span class="mmm">+30</span><br>минут</p></div>
                    <div onclick="select_time(4)" id="minuty4" class="minuty" style="color: #414042; background: #f4f4f4;"><p><span class="mmm">+45</span><br>минут</p></div>
                    <div class="clear"></div>
                </div>
                	
                <div class="sel_time2" >
                    <input placeholder="Время подачи" class="time_sel" value="" onclick="NewCssCal('CalltaxiForm_OrderTime', 'yyyyMMdd', 'arrow', true, '24', false);" name="ORDER_TIME" id="CalltaxiForm_OrderTime" type="text">
                    <div class="clear"></div>
                </div>
			</div>
			<div class="cnt_3_1_1">
				<label>Дополнительные требования:</label>
				<select class="" name="sposoby_opl" id="sposoby_opl">
                    <option value="">Выберите требование</option>
	                <option value="Встреча с табличкой (+ 200 тг)">Встреча с табличкой (+ 200 тг)</option>
	                <option value="Услуга трезвый водитель (+ 1 200 тг)">Услуга трезвый водитель (+ 1 200 тг)</option>
	                <option value="Багаж в салоне (+ 200 тг)">Багаж в салоне (+ 200 тг)</option>
	                <option value="Поездка с животными (бесплатно)">Поездка с животными (бесплатно)</option>
	                <option value="Брендированный автомобиль (бесплатно)">Брендированный автомобиль (бесплатно)</option>
	                <option value="Молчаливый водитель (бесплатно)">Молчаливый водитель (бесплатно)</option>
	                <option value="Деликатное вождение (бесплатно)">Деликатное вождение (бесплатно)</option>
	            </select>
			</div>
			<div class="cnt_3_1_1">
				<label>Комментарии:</label>
				<textarea value="" class="" placeholder="Тут можно указать где Вас ожидать..." name="COMMENT" id="COMMENT" maxlength="400"></textarea>
			</div>
		</div>
		<div class="cnt_3_2">
			<div class="cnt_3_2_1 activ_car" id="sedan">
				<div class="cnt_3_2_1_t_1">Toyota Corolla</div>
				<img src="img/corolla.png" style="    margin-top: -50px;" class="cnt_3_2_1_i">
				<div class="cnt_3_2_1_t_2" style="    margin-top: 55px;">1км - 50тг</div>
				<div class="cnt_3_2_1_t_3">от <div class="cnt_3_2_1_t_4">400</div> 250 тенге</div>
				
			</div>
			<div id="sedan_text" class="cnt_3_2_1_add">
				Такси Колёса - беспрецедентно смелый и динамичный автомобиль Тойота Королла  2012 года
			</div>
			<div class="cnt_3_2_1" id="crossover">
				<div class="cnt_3_2_1_t_1">Toyota Camry</div>
				<img src="img/2011-toyota-camry.png" style="width: 70%;    margin: -35px 5px -40px 0px;" class="cnt_3_2_1_i">
				<div class="cnt_3_2_1_t_2" style="    margin-top: 60px;">1км - 75тг</div>
				<div class="cnt_3_2_1_t_3">от <div class="cnt_3_2_1_t_4">500</div> 300 тенге</div>
			</div>
			<div id="crossover_text" class="cnt_3_2_1_add">
				Такси Колёса – надежный и комфортабельный  бизнес класса  Тойота Камри  2012 года
			</div>
			<div class="cnt_3_2_1" id="vip">
				<div class="cnt_3_2_1_t_1">Infinity Q 56</div>
				<img src="img/inf.png" style="    margin: -30px 0px -40px 0px;" class="cnt_3_2_1_i">
				<div class="cnt_3_2_1_t_2" style="    margin-top: 60px;">VIP</div>
				<div class="cnt_3_2_1_t_3">1час <div class="cnt_3_2_1_t_4">20 000</div> 14 000 тенге</div>
			</div>
			<div id="vip_text" class="cnt_3_2_1_add">
				Такси Колёса VIP – автомобиль премиум  класса маркой  Infiniti, Lexus, Mercedez-Benz, BMW
			</div>
			<input type="button" value="Заказать"  id="submit_client" class="cnt_3_2_1_btn" name="send">
		</div>
		<div class="cnt_3_3">
		<img src="img/10skidka.png" class="sale">
                <div id="YMapsID" style="width:390px;   display: inline-block; height: 535px;  padding: 0px 0;  overflow: hidden;"></div>
                <div id="coord_form">
                    <input id="latlongmet" class="input-medium" name="icon_text1" type="hidden">
                    <input id="latlongmet2" class="input-medium" name="icon_text2" type="hidden">
                    <input id="crew_id" class="input-medium" name="icon_text3" type="hidden">
            </div>
		</div>
		</form>
	</div>

	<div class="cnt_4" name="cnt_tarify">
		<div class="cnt_4_1">Наши цены на рынке самые выгодные!</div>
		<div class="cnt_4_2">
			<div class="cnt_4_2_1">
				<div class="cnt_4_2_1_1"></div>
				<div class="cnt_4_2_1_2"><img src="img/echo_taxi.png"></div>
				<div class="cnt_4_2_1_3"><img src="img/logo_econom.png"></div>
				<div class="cnt_4_2_1_4"><img src="img/logo-leader.png"></div>
				<div class="cnt_4_2_1_5"><img src="img/logo5353.png"></div>
				<div class="cnt_4_2_1_6"><img src="img/logo_taxi_kolesa.png"></div>
			</div>
			<div class="cnt_4_2_2">
				<div class="cnt_4_2_1_1">Такси по городу</div>
				<div class="cnt_4_2_1_2"><img src="img/echo_taxi.png" class="cnt_4_2_2_i">от 590 тг</div>
				<div class="cnt_4_2_1_3"><img src="img/logo_econom.png" class="cnt_4_2_2_i">от 400 тг</div>
				<div class="cnt_4_2_1_4"><img src="img/logo-leader.png" class="cnt_4_2_2_i">от 400 тг</div>
				<div class="cnt_4_2_1_5"><img src="img/logo5353.png" class="cnt_4_2_2_i">от 500 тг</div>
				<div class="cnt_4_2_1_6"><img src="img/logo_taxi_kolesa.png" class="cnt_4_2_2_i">от 300 тг</div>
			</div>
			<div class="cnt_4_2_3">
				<div class="cnt_4_2_1_1">Услуга трезвого водителя </div>
				<div class="cnt_4_2_1_2"><img src="img/echo_taxi.png" class="cnt_4_2_2_i">нет</div>
				<div class="cnt_4_2_1_3"><img src="img/logo_econom.png" class="cnt_4_2_2_i">от 1 200 тг</div>
				<div class="cnt_4_2_1_4"><img src="img/logo-leader.png" class="cnt_4_2_2_i">от 2 500 тг</div>
				<div class="cnt_4_2_1_5"><img src="img/logo5353.png" class="cnt_4_2_2_i">от 2 000 тг</div>
				<div class="cnt_4_2_1_6"><img src="img/logo_taxi_kolesa.png" class="cnt_4_2_2_i">от 1 200 тг</div>
			</div>
			<div class="cnt_4_2_4">
				<div class="cnt_4_2_1_1">Центр города - Аэропорт</div>
				<div class="cnt_4_2_1_2"><img src="img/echo_taxi.png" class="cnt_4_2_2_i">от 2 500 тг</div>
				<div class="cnt_4_2_1_3"><img src="img/logo_econom.png" class="cnt_4_2_2_i">от 1 200 тг</div>
				<div class="cnt_4_2_1_4"><img src="img/logo-leader.png" class="cnt_4_2_2_i">от 1 500 тг</div>
				<div class="cnt_4_2_1_5"><img src="img/logo5353.png" class="cnt_4_2_2_i">от 1 600 тг</div>
				<div class="cnt_4_2_1_6"><img src="img/logo_taxi_kolesa.png" class="cnt_4_2_2_i">от 1 100 тг</div>
			</div>
			<div class="cnt_4_2_5">
				<div class="cnt_4_2_1_1">г.Алматы – г.Капчагай</div>
				<div class="cnt_4_2_1_2"><img src="img/echo_taxi.png" class="cnt_4_2_2_i">от 4 500 тг</div>
				<div class="cnt_4_2_1_3"><img src="img/logo_econom.png" class="cnt_4_2_2_i">от 4 500 тг</div>
				<div class="cnt_4_2_1_4"><img src="img/logo-leader.png" class="cnt_4_2_2_i">от 4 800 тг</div>
				<div class="cnt_4_2_1_5"><img src="img/logo5353.png" class="cnt_4_2_2_i">от 6 000 тг</div>
				<div class="cnt_4_2_1_6"><img src="img/logo_taxi_kolesa.png" class="cnt_4_2_2_i">от 4 000 тг</div>
			</div>
			<div class="cnt_4_2_6">
				<div class="cnt_4_2_1_1">г.Алматы – зоны отдыха Капчагая</div>
				<div class="cnt_4_2_1_2"><img src="img/echo_taxi.png" class="cnt_4_2_2_i">от 5 500 тг</div>
				<div class="cnt_4_2_1_3"><img src="img/logo_econom.png" class="cnt_4_2_2_i">от 6 500 тг</div>
				<div class="cnt_4_2_1_4"><img src="img/logo-leader.png" class="cnt_4_2_2_i">нет</div>
				<div class="cnt_4_2_1_5"><img src="img/logo5353.png" class="cnt_4_2_2_i">от 6 600 тг</div>
				<div class="cnt_4_2_1_6"><img src="img/logo_taxi_kolesa.png" class="cnt_4_2_2_i">от 5 400 тг</div>
			</div>
			<div class="cnt_4_2_7">
				<div class="cnt_4_2_1_1">г.Алматы – г.Каскелен</div>
				<div class="cnt_4_2_1_2"><img src="img/echo_taxi.png" class="cnt_4_2_2_i">от 1 250 тг</div>
				<div class="cnt_4_2_1_3"><img src="img/logo_econom.png" class="cnt_4_2_2_i">от 2 000 тг</div>
				<div class="cnt_4_2_1_4"><img src="img/logo-leader.png" class="cnt_4_2_2_i">от 1 900 тг</div>
				<div class="cnt_4_2_1_5"><img src="img/logo5353.png" class="cnt_4_2_2_i">от 2 500 тг</div>
				<div class="cnt_4_2_1_6"><img src="img/logo_taxi_kolesa.png" class="cnt_4_2_2_i">от 1 100 тг</div>
			</div>
		</div>
	</div>

	<div class="cnt_5" name="cnt_otzyvy">
		<div class="cnt_5_1">О НАС ГОВОРЯТ</div>
		<div class="cnt_5_2">
			<video width="290" height="210" controls>
			  <source src="video/1.mp4" type="video/mp4">
			</video>
		</div>
		<div class="cnt_5_2">
			<video width="290" height="210" controls>
			  <source src="video/2.mp4" type="video/mp4">
			</video>
		</div>
		<div class="cnt_5_2">
			<video width="290" height="210" controls>
			  <source src="video/3.mp4" type="video/mp4">
			</video>
		</div>
		<div class="cnt_5_2">
			<video width="290" height="210" controls>
			  <source src="video/4.mp4" type="video/mp4">
			</video>
		</div>
		<div class="cnt_5_2">
			<video width="290" height="210" controls>
			  <source src="video/5.mp4" type="video/mp4">
			</video>
		</div>
	</div>

	<div class="cnt_7" name="cnt_voditelyam">
		<div class="cnt_7_1">Набор водителей</div>
		<div class="cnt_7_2">
			
			Мы постоянно проводим набор водителей в нашу службу такси. Прием водителей проводится каждый будний день с 09:00 до 18:00 по адресу:
			г. Алматы, пр. Аль-Фараби 71/21, здание Керемет КазНУ им. Аль-Фараби.
			<br /><br />
			<div class="cnt_7_2_b">Требования для водителей:</div>
			- наличие личного автомобиля любой марки<br />
			- средне-специальное образование<br />
			- водительские права категории “В”<br />
			- стаж вождения более 3 лет<br />
			<br />
			<div class="cnt_7_2_b">Мы предлагаем:</div>
			- свободный график работы<br />
			- бонусы и акции<br />
			- стабильный доход 
			<br /><br />
			<div class="cnt_7_2_b">Если хотите работать и зарабатывать, звоните и записывайтесь на собеседование.</div>
			<div class="cnt_7_2_b">+7 (727) 221-30-31</div>
			<div class="cnt_7_2_b">+7 (727) 221-30-32</div>
			<div class="cnt_7_2_b">+7 (701) 503-71-70</div>
			<div class="cnt_7_2_b">+7 (701) 813-00-43</div>
		</div>
	</div>

	<div class="cnt_8" name="cnt_korporat">
		<div class="cnt_7_1">Корпоративным клиентам</div>
		<div class="cnt_7_2">
			
			

			Компания «Такси-Колеса» предлагает услуги такси по обслуживанию корпоративных клиентов по основным направлениям: <br /><br />

			- деловые поездки, обслуживание удаленных филиалов; <br />
			- трансферы в аэропорт и на железнодорожный вокзал;<br />
			- встреча деловых партнеров и важных гостей;<br />
			-  курьерские услуги<br /><br />

			Почему Вам стоить выбрать Нас?<br /><br />
			<div class="cnt_7_2_b">Гарантированная подача в назначенное время</div>
			Сотрудничество оформляется строго по договору. Вам не придется оплачивать лишние издержки, потому что мы сами отслеживаем изменения в расписаниях аэропортов и вокзалов. <br /><br />

			<div class="cnt_7_2_b">Сначала пользуйтесь,  потом платите </div>
			Предоплата нулевая. Мы осуществляем корпоративные перевозки на постоплатной системе взаиморасчетов. 
			Ваш персональный менеджер<br />
			Вам будет предоставлен специалист, который сможет ответить на Ваши вопросы 24/7. <br /><br />

			<div class="cnt_7_2_b">Долой очередь</div>
			Заказы корпоративных клиентов принимаются в первую очередь, поэтому Вам не придется ждать длинных гудков. <br /><br />

			<div class="cnt_7_2_b">Гибкая и четкая ценовая политика</div>
			Мы предлагаем заказ автомобилей бизнес класса по доступным тарифам. Постоянные клиенты получают хорошие скидки. 

		</div>
	</div>

	<div class="cnt_6" name="cnt_contakty">
		<div class="cnt_6_1">КОНТАКТЫ</div>
		<div class="cnt_6_2">
			<div class="cnt_6_2_1">
				<div class="cnt_6_2_1_1">Мы в социальных сетях</div>
				<div class="cnt_6_2_1_2">
					<a target="_blank" href="http://vk.com/id297311396"><img class="scl" src="img/vk-64.png" alt="vkontakte"></a>
					<a target="_blank" href="https://www.facebook.com/profile.php?id=100009004909805&fref=ts"><img class="scl" src="img/facebook-60.png" alt="facebook"></a>
					<a target="_blank" href="https://instagram.com/taxikolesa_almaty/"><img class="scl" src="img/instagram-60.png" alt="instagram"></a>
				</div>
			</div>
			<div class="cnt_6_2_1">
				<div class="cnt_6_2_1_1">Звоните нам</div>
				<div class="cnt_6_2_1_2">
					<img src="img/24.png" alt="24 часа" class="hour24" style="    float: left;    margin: 0 -60px 0 20px;"><div>+7 (727) 338-77-77</div>
					<div>+7 (702) 221-30-30</div>
				</div>
			</div>
			<div class="cnt_6_2_1">
				<div class="cnt_6_2_1_1">Наше приложение</div>
				<div class="cnt_6_2_1_2">
					<a target="_blank" href="http://est4u.org/1Q6lArmj"><img class="app" src="img/appstore.png" alt="app store"></a>
					<a target="_blank" href="http://est4u.org/1Q6lArmj"><img class="app" src="img/googleplay.png" alt="google play"></a>
				</div>
			</div>
		</div>
	</div>
</div>

<img src="img/zvonok.png" class="zvonok" alt="Звонок">

<!--
            <div class="col-sm-12">
                <div class="row">
					<div class="col-sm-2">

-->


<!--			GOOGLE			-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	
  ga('set', 'contentGroup1', 'My Group Name'); 
  ga('create', 'UA-64491732-1', 'auto');
  ga('send', 'pageview');

</script>
<!--		END	GOOGLE			-->

<!-- 		facebook 			-->
<script>(function() {
var _fbq = window._fbq || (window._fbq = []);
if (!_fbq.loaded) {
var fbds = document.createElement('script');
fbds.async = true;
fbds.src = '//connect.facebook.net/en_US/fbds.js';
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(fbds, s);
_fbq.loaded = true;
}
_fbq.push(['addPixelId', '990348620983459']);
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', 'PixelInitialized', {}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=990348620983459&am.." /></noscript>
<!--		END	facebook 			-->

<!-- 		VK 			-->
<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=ZiCm4/WcOHu*r*i0yKgr9ak21p2mZvOXltOpCGecqAizcpu2VyCNoZRbC3hCQYUNQHOW/2E*x9SbyeS4nA1BF3Lt7zZD0CeTwUf6Qp5xJ1aDhx4Q8/X0215R4TpQAfsjgxIZaFHtypz4ZThtY5yQpBYhhCVnwEiF/0XfJSvVW48-';</script>
<!-- 	END	VK 			-->

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter31046886 = new Ya.Metrika({
                    id:31046886,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/31046886" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
</body>
</html>