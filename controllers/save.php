<?php

function save_index()
{
	$emails = array(
		'subaur@mail.ru',
		'alexey_12@list.ru',
	);


	$errors = array();

	if ( ! $_POST['name'] ) $errors['name'] = 'name';
	if ( ! $_POST['phone'] ) $errors['phone'] = 'phone';

	if ( ! count($errors))
	{
		$title = 'Заявка с сайта КазПромАрм.';

		$text = 'Заявка с сайта КазПромАрм.<br />';
		$text .= 'Имя - '.$_POST['name'].'<br />';
		$text .= 'Телефон - '.$_POST['phone'].'<br />';
		$text .= ($_POST['email']) ? 'E-mail - '.$_POST['email'].'<br />' : '';
		$text .= '<br />Точка захвата - '.$_POST['point'];

		foreach ($emails as $e) {
			smtpmail($e, $title, $text);
		}
	}
}