<?php

function render_template($view, $data)
{
	$content = render_view($view, $data);

	$menu = route_controller();
	if ($menu == 'page')
	{
		$id = route_param();
		$menu = 'page'.$id;
	}
	else if ($menu == 'item')
	{
		$menu = 'catalog';
	}

	$template_data = array(
		'content'	=> $content,
		'menu'		=> $menu,
	);
	echo render_view('templates/main', $template_data);
}