<?php

if ( ! user_is_admin()) go(base_url().'admin/enter');

function render_template($view, $view_data)
{
	$content = render_view($view, $view_data);
	$data = array(
		'content'		=> $content,
		'user'			=> $_SESSION['user'],
		'title'			=> array_get('title', $view_data, 'Админка')
	);

	echo render_view('templates/admin', $data);
}

?>
