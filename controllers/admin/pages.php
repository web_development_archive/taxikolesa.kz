<?php

function admin_pages_index()
{
	require_once('controllers/templates/admin.php');

	$data = array(
		'title'		=> 'Страницы',
		'pages'		=> page_get_all(),
		'page_id'	=> array_get('page', $_GET),
	);

	render_template('admin/pages', $data);
}

function admin_pages_save()
{
	$page = page_save($_POST);
	go(base_url().'admin/pages?page='.$page['id']);
}