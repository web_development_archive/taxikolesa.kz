<?php

function admin_items_index()
{
	require_once('controllers/templates/admin.php');

	$categories = array();

	$items = item_get_all();
	$items = array_reverse($items);

	$data = array(
		'title'			=> 'Товары',
		'items'			=> $items,
	);

	render_template('admin/items', $data);
}

function admin_items_save()
{
	$item = item_save($_POST);

	go(base_url().'admin/items');
}

function admin_items_addPhoto()
{
	echo photo_add($_FILES['photo'], $_POST['model_id'], $_POST['model']);
}

function admin_items_deletePhoto($photo_id)
{
	photo_delete(photo_get_by('id', $photo_id));
}

function admin_items_delete($item_id)
{
	$item = item_get_by('id', $item_id);
	item_delete($item);
	go(base_url().'admin/items');
}