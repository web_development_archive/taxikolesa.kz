<?php

function admin_enter_index()
{
	if (user_is_admin()) go(base_url().'admin/index');

	if (isset($_POST['submit_login']))
	{
		if (user_login($_POST['username'], $_POST['password']))
		{
			go(base_url().'admin/index');
		}
	}

	$data = array(
		'username'	=> array_get('username', $_POST, ''),
	);

	echo render_view('admin/enter', $data);
}