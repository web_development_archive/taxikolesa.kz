<?php

function admin_index_index()
{
	require_once('controllers/templates/admin.php');

	$data = array(
		'title'		=> 'Главная',
	);

	render_template('admin/index', $data);
}

function admin_index_logout()
{
	user_logout();
	go_back();
}