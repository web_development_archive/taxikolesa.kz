$(document).ready(function () {

    $('#submit_call').on('click', function() {
        zakaz_zvonka();
    });

    $('.popup .close_window, .overlay').click(function (){
        $('.popup, .overlay').css('opacity','0');
        $('.popup, .overlay').css('visibility','hidden');
    });
    $('.zvonok').click(function (e){
        $('.popup, .overlay').css('opacity','1');
        $('.popup, .overlay').css('visibility','visible');
        e.preventDefault();
    });

    $( 'a[href^=#cnt_]' ).click( function () {
        var id = $( this ).attr( 'href' ).match( /#.+$/ )[0].substr(1);
        var offsetTop = $("div[name='"+ id + "']").offset().top;
        $('body,html').animate({scrollTop:offsetTop}, 500);
        return false;
    });
    $(".cnt_3_1_1_3").click(function(){
        $(".cnt_3_1_1_2").addClass("activ_time_t");
        $(".cnt_3_1_1_3").removeClass("activ_time_t");
        $(".sel_time2").css("display","inline-block");
        $(".sel_time").css("display","none");
    });
    $(".cnt_3_1_1_2").click(function(){
        $(".cnt_3_1_1_3").addClass("activ_time_t");
        $(".cnt_3_1_1_2").removeClass("activ_time_t");
        $(".sel_time").css("display","inline-block");
        $(".sel_time2").css("display","none");
    });

    var w = screen.width,
    h = screen.height;
    if(w < 1200 ){
        $('#wrapper').css('width', '1200px');
        $('#header').css('width', '1200px');
        $('#content').css('width', '1200px');
        $('#footer').css('width', '1200px');
    }
    
    $('#nav li').hover(
        function () {
            var coc_city = getCookie('gorod_landing');
            if (coc_city == 'almaty'){
                $('ul', this).slideDown(100); 
            } 

        },
        function () {
            var coc_city = getCookie('gorod_landing');
            if (coc_city == 'almaty'){
                $('ul', this).slideUp(100);
            } 
        }
    );
    
    var onas = $("#onas");
    var tarif = $('#tarif');
    var korp = $('#korp');
    var glavn = $('#glavn');
    var personal_area = $('#personal_area');
    //glavn.hide();
    onas.hide();
    tarif.hide();
    korp.hide();
    personal_area.hide();
    
    view_map_param = 0;
    mas_current_crew=[];
            
    $('#wrapper').click(function(){
        var callborder = $('#calBorder').css('display');
//        alert(callborder);
        if(typeof callborder != "undefined"){
           //$('#calBorder').css('display', 'none'); 
        }
    })
    
    var sedan = $("#sedan1");
    var crossover = $("#crossover1");
    var vip = $("#vip1");
    
    cls_mashiny = 'sedan';
    vybor_time = 0;
    vyb_pod = 'pl';
    sort_mass = [];
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var hours = d.getHours();
    var minutes = d.getMinutes();
    var output = d.getFullYear() + '.' + (('' + month).length < 2 ? '0' : '') + month + '.' + (('' + day).length < 2 ? '0' : '') + day + ' ' + hours + ':' + minutes;
    date_time = output;
           
    $('a#link1').click(function(){
        animateDiv(onas, tarif, korp, glavn);
    });
    $('a#link2').click(function(){
        animateDiv(tarif, korp, onas, glavn);
    });
    $('a#link3').click(function(){
        animateDiv(korp, tarif, onas, glavn);
    });
    $('a#linkhome').click(function(){
        document.location.href = "/";
        //animateDiv(glavn, korp, tarif, onas);
    });
    
    $('#linkhome2').click(function(){
        animateDiv(glavn, korp, tarif, onas);
    });
    $('a#zvonok_obr').click(function(){
        $('.forma_obr').slideToggle();
    });
    
    $('#obmen_toch').click(function(){
        var a1 = $('#CalltaxiForm_OrderSourceAddress').val();
        var a2 = $('#CalltaxiForm_SourceAddress').val();
        $('#CalltaxiForm_OrderSourceAddress').val(a2);
        $('#CalltaxiForm_SourceAddress').val(a1);
    });
    
    $('.obrrr').click(function(){
        event.preventDefault();
        
        contact_nomer = $('#nishtyak').val();
        contack_message = "Клиент просить позвонить на этот номер + +" + contact_nomer;
        if(contact_nomer != ''){
            dataString = contack_message;
            sendMail(dataString);
        }
        else
        {
            alert('Введите номер телефона');
        }
    });
    
    $('.zakaz_z').click(function(){
        event.preventDefault();
        
        contact_nomer = $('.tell_z').val();
        contack_name = $('.name_z').val()
        contack_message = "Вас просить позвонить по вопросу сотрудничеству по тел. + +" + contact_nomer + ". Имя: " + contack_name;
        if(contact_nomer != ''){
            dataString = contack_message;
            //sendMail(dataString);
        }
        else
        {
            alert('Введите номер телефона');
        }
    });


    jQuery(function($){
       $("#PHONE_TO_DIAL, #nishtyak, .tell_z").mask("+7 (999) 999-9999");
    });
    // $('#CalltaxiForm_Number').mask('99999999999'); // Маска для телефона
   /* $("#PHONE_TO_DIAL").keydown(function(event) {
        // Разрешаем: backspace, delete, tab и escape
        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || 
            // Разрешаем: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) || 
            // Разрешаем: home, end, влево, вправо
            (event.keyCode >= 35 && event.keyCode <= 39)) {
            // Ничего не делаем
            return;
        }
        else {
            // Обеждаемся, что это цифра, и останавливаем событие keypress
            if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault(); 
            }   
        }
    });                  */
    class_mashiny(sedan, crossover, vip);
    change_time(1);
    select_time(1);
    var coc_city = getCookie('gorod_landing');
    if (coc_city == 'almaty'){
  		$('.changer2').css('left', '101');
        $('#gorod11').css('color','white');
        $('#gorod22').css('color','#BCBEC0');
        $('.changer2').css('border-bottom-right-radius','12px');
        $('.changer2').css('border-top-right-radius','12px');
        $('.changer2').css('border-bottom-left-radius','0');
        $('.changer2').css('border-top-left-radius','0');
        $('.changer2').css('background', '#2E9441');
        $('.tarify, .tarify2').css('display', 'inherit');
        $('.tarify_ast').css('display', 'none');
        $('.telefon_nomer_dlya_zv1').html("+7 (727) 3 900 500");
        $('.telefon_nomer_dlya_zv2').html("+7 (727) 672 77 77");
        // класс машины
        $('#sedan').click(function(){
            cls_mashiny = 'sedan';
            class_mashiny(sedan, crossover, vip);
            $('#sedan').addClass("activ_car");
            $('#sedan_text').css("display","block");
            $('#crossover_text').css("display","none");
            $('#vip_text').css("display","none");
            $("#vip").removeClass("activ_car");
            $('#crossover').removeClass("activ_car");
            /*$('.vip_icon').html('<img src="img/info2.png"/>');
            $('.cross_icon').html('<img src="img/info2.png"/>');
            $('.sedan_icon').html('<img src="img/info.png"/>');*/
        });
        $('#crossover').click(function(){
            cls_mashiny = 'crossover';
            class_mashiny(crossover, sedan, vip);
            $('#crossover_text').css("display","block");
            $('#vip_text').css("display","none");
            $('#sedan_text').css("display","none");
            $('#crossover').addClass("activ_car");
            $("#vip").removeClass("activ_car");
            $('#sedan').removeClass("activ_car");

            /*
            $('.vip_icon').html('<img src="img/info2.png"/>');
            $('.cross_icon').html('<img src="img/info.png"/>');
            $('.sedan_icon').html('<img src="img/info2.png"/>');*/
        });
        
        $('#vip').click(function(){
            cls_mashiny = 'vip';
            class_mashiny(vip, sedan, crossover);
            $("#vip").addClass("activ_car");
            $('#vip_text').css("display","block");
            $('#crossover_text').css("display","none");
            $('#sedan_text').css("display","none");
            $('#crossover').removeClass("activ_car");
            $('#sedan').removeClass("activ_car");
            /*
            $('.vip_icon').html('<img src="img/info.png"/>');
            $('.cross_icon').html('<img src="img/info2.png"/>');
            $('.sedan_icon').html('<img src="img/info2.png"/>');*/
        });
          
    }
    else{
        document.cookie = "gorod_landing=almaty";
        change_gorod_menu('Алматы');
    }
    
    $('#zakaz_taxi1').show();
    $('#data_com1').hide();
    $('#curr_order1').hide();
    $('#history_order1').hide();
    $('#otzyv1').hide();
    
    $('#hist_show').click(function(e){
        e.preventDefault();
        if($('#time_start').val() != "" && $('#time_finish').val() != ""){
            get_history_show();
        }
    })
    
    $('#zakaz_taxi').click(function(e){
        e.preventDefault();
        $("#ztm").html('<img src="img/menu/m12.png"/>');
        $("#dcm").html('<img src="img/menu/m21.png"/>');
        $("#com").html('<img src="img/menu/m31.png"/>');
        $("#hom").html('<img src="img/menu/m41.png"/>');
        $("#om").html('<img src="img/menu/m51.png"/>');
        
        $('#zakaz_taxi1').show();
        $('#data_com1').hide();
        $('#curr_order1').hide();
        $('#history_order1').hide();
        $('#otzyv1').hide();
    })
    
    $('#data_com').click(function(e){
        e.preventDefault();
        $("#ztm").html('<img src="img/menu/m11.png"/>');
        $("#dcm").html('<img src="img/menu/m22.png"/>');
        $("#com").html('<img src="img/menu/m31.png"/>');
        $("#hom").html('<img src="img/menu/m41.png"/>');
        $("#om").html('<img src="img/menu/m51.png"/>');
        
        $('#zakaz_taxi1').hide();
        $('#data_com1').show();
        $('#curr_order1').hide();
        $('#history_order1').hide();
        $('#otzyv1').hide();
    })
    
    $('#curr_order').click(function(e){
        e.preventDefault();
        $("#ztm").html('<img src="img/menu/m11.png"/>');
        $("#dcm").html('<img src="img/menu/m21.png"/>');
        $("#com").html('<img src="img/menu/m32.png"/>');
        $("#hom").html('<img src="img/menu/m41.png"/>');
        $("#om").html('<img src="img/menu/m51.png"/>');
        
        $('#zakaz_taxi1').hide();
        $('#data_com1').hide();
        $('#curr_order1').show();
        $('#history_order1').hide();
        $('#otzyv1').hide();
        
        get_current_orders();
    })
    
    $('#history_order').click(function(e){
        e.preventDefault();
        $("#ztm").html('<img src="img/menu/m11.png"/>');
        $("#dcm").html('<img src="img/menu/m21.png"/>');
        $("#com").html('<img src="img/menu/m31.png"/>');
        $("#hom").html('<img src="img/menu/m42.png"/>');
        $("#om").html('<img src="img/menu/m51.png"/>');
        
        $('#zakaz_taxi1').hide();
        $('#data_com1').hide();
        $('#curr_order1').hide();
        $('#history_order1').show();
        $('#otzyv1').hide();
    })
    
    $('#otzyv').click(function(e){
        e.preventDefault();
        $("#ztm").html('<img src="img/menu/m11.png"/>');
        $("#dcm").html('<img src="img/menu/m21.png"/>');
        $("#com").html('<img src="img/menu/m31.png"/>');
        $("#hom").html('<img src="img/menu/m41.png"/>');
        $("#om").html('<img src="img/menu/m52.png"/>');
        
        $('#zakaz_taxi1').hide();
        $('#data_com1').hide();
        $('#curr_order1').hide();
        $('#history_order1').hide();
        $('#otzyv1').show();
    })
    
    $('#exit_area').click(function(e){
        e.preventDefault();
        var url = "exit.php";
        $.ajax({
            type: "GET",
            url: url,
            dataType: "html",
            cache: false,
            success: function(data) {
                $("#ztm").html('<img src="img/menu/m11.png"/>');
                $("#dcm").html('<img src="img/menu/m21.png"/>');
                $("#com").html('<img src="img/menu/m31.png"/>');
                $("#hom").html('<img src="img/menu/m41.png"/>');
                $("#om").html('<img src="img/menu/m51.png"/>');
                document.location.href = "/";
            }
        })
    })
    
    $('.personal_area2').click(function(e){
        document.location.href = "personal_area.php";
    })
    
    $('.personal_area').click(function(e){
        e.preventDefault();
        $('#error_code_100').css('visibility', 'hidden');
        $('#yandex_map').css('display', 'none');
        //$('.reg_class').animate({'left':'40%'},550);
        $('#parent_reg').css('display', 'inherit');
        $('#open-close_form').click(function(event) {
            event.preventDefault();
            $('#yandex_map').css('display', 'inherit');
            $('#parent_reg').css('display', 'none');
        });
        $('#go_to_personal_area').click(function(event) {
            event.preventDefault();
            var login = $('#login').val();
            var password = $('#password').val();
            var customer_password = $('#customer_password').val();
            var url = "check_author.php?login="+login+"&password="+password+"&customer_password="+customer_password;
            $.ajax({
                type: "GET",
                url: url,
                dataType: "html",
                cache: false,
                success: function(data) {
                    var jsonData = $.parseJSON(data);
                    //console.log(jsonData);
                    if(jsonData['customer'] == 0){
                        $('#error_code_100').css('visibility', 'visible');
                    }
                    else{
                        $('#error_code_100').css('visibility', 'hidden');
                        $('#parent_reg').css('display', 'none');
                        document.cookie = "gorod_landing="+jsonData['city'];
                        document.location.href = "personal_area.php";
                    }
                }
            })
        });
    })
    
    $('#submit_client22').click(function(e){
        e.preventDefault();
        
        $('.input_for_area3').css('border', '2px solid #2D9136');
        $('.region3').css('border', '2px solid #2D9136'); 
            
        var email_client = $('#email_client').val();
        var name_client = $('#name_client').val();
        var comment_client = $('#comment_client').val();
    
        contack_message = "E-mail: " + email_client + "\n\r Имя клиента: " + name_client + "\n\r Сообщение: " + comment_client;
        if(email_client != "" && name_client != "" && comment_client != ""){
            var em_check = checkmail(email_client);
            if(em_check == "1"){
                dataString = contack_message;
                sendMail3(dataString);
            }
            else{
                $('#region_email').css('border', '3px solid red');
            }
        }
        else
        {
            if(email_client == ""){
                $('#region_email').css('border', '3px solid red');
            }
            if(name_client == ""){
                $('#region_names').css('border', '3px solid red');
            }
            if(comment_client == ""){
                $('.input_for_area3').css('border', '3px solid red');
            }   
        }
    })
    
    
    
    var myMap, myPlacemark, myMap2, myPlacemark2;
    coords = null;
    coords2 = null;
    route = null;
    markers = [null, null];
    placemark1 = 0;
    placemark2 = 0;
    tochka = 0;
    points = []; 
    c = true;
    if($('#YMapsID').length != 0){
        ymaps.ready(init);
    }
    else{
        
    }
	$('#submit_client').on('click', function() {
		submit_order_form_client();
	});
});

function init () {
    var coc_city = getCookie('gorod_landing');
    gorod = [43.23912236, 76.92015652];
    gorod222 = [43.24597755, 76.86321858];
    
    aeroport_kord = [43.34668833, 77.01212410];
    vokzal_kord =[43.34101467, 76.94895437];
    almaty1 = [43.34101467, 76.94895437];
    almaty2 = [43.27378007, 76.93911289];
    
    text_gorod = "Казахстан, Алматы, ";
    
    //Определяем начальные параметры карты
    myMap = new ymaps.Map('YMapsID', {
        center: gorod, 
        zoom: 12,
        behaviors:['default', 'scrollZoom']
    }); 
    //Определяем элемент управления поиск по карте  
    var SearchControl = new ymaps.control.SearchControl({noPlacemark:true});    
 
    //Добавляем элементы управления на карту
    myMap.controls                        
    .add('zoomControl');
        
    $('#samolet1').click(function(){
        coords = aeroport_kord;
        if(typeof(myPlacemark) != "undefined"){
            myMap.geoObjects.remove(myPlacemark);
        }
                myPlacemark = new ymaps.Placemark(aeroport_kord,{hintContent: 'Откуда едем'},
                {
                    iconLayout: 'default#image',
                    iconImageHref: '../img/a.png',
                    iconImageSize: [50, 65],
                    iconImageOffset: [-25, -32],
                    draggable: true
                }); 
                myMap.geoObjects.add(myPlacemark);
                
                placemark1 = 1;
                ymaps.geocode(myPlacemark.geometry.getCoordinates(), {
                    results: 1
                }).then(function (res) {
                    if(placemark2 == 1 && placemark1 == 1){
                        var new_cent_lat = (Number(myPlacemark.geometry.getCoordinates()[0]) + Number(myPlacemark2.geometry.getCoordinates()[0]))/2;
                        var new_cent_lon = (Number(myPlacemark.geometry.getCoordinates()[1]) + Number(myPlacemark2.geometry.getCoordinates()[1]))/2;
                        var new_center = [];
                        new_center.push(new_cent_lat);
                        new_center.push(new_cent_lon);
                        console.log(new_center);
                        myMap.setCenter(new_center, 12, {
    
                        });
                    }
                    else{
                        var firstGeoObject = res.geoObjects.get(0),
                            coords = firstGeoObject.geometry.getCoordinates(),
                            bounds = firstGeoObject.properties.get('boundedBy');
                        myMap.setCenter(coords, 12, {
    
                        });
                    }
                });
                markers[0] = myPlacemark;
                document.getElementById("latlongmet").value = myPlacemark.geometry.getCoordinates();
                show_adress_list();
                if(placemark2 == 1 && placemark1 == 1){
                    calcRoute();
                }
                 //Отслеживаем событие перемещения метки
                myPlacemark.events.add("dragend", function (e) {
                    coords = this.geometry.getCoordinates();
                    savecoordinats();
                    document.getElementById("message2").style.display='none';
                    document.getElementById("latlongmet").value = coords;
                    show_adress_list();
                    if(placemark2 == 1 && placemark1 == 1){
                        myMap.geoObjects.remove(route.getPaths());
                        reset();
                        calcRoute();
                    }
                }, myPlacemark);
                
                //Отслеживаем событие выбора результата поиска
//                SearchControl.events.add("resultselect", function (e) {
//                    coords = SearchControl.getResultsArray()[0].geometry.getCoordinates();
//                    savecoordinats();
//                    document.getElementById("latlongmet").value = coords;
//                    show_adress_list();
//                });
                $('#CalltaxiForm_OrderSourceAddress').val("Аэропорт");
    });
    
    
    $('#samolet2').click(function(){
        coords2 = aeroport_kord;
        if(typeof(myPlacemark2) != "undefined"){
            myMap.geoObjects.remove(myPlacemark2);
        }
           myPlacemark2 = new ymaps.Placemark(aeroport_kord,{hintContent: 'Куда едем'},
            {
                iconLayout: 'default#image',
                iconImageHref: '../img/b.png',
                iconImageSize: [50, 65],
                iconImageOffset: [-25, -32],
                draggable: true
            }); 
            myMap.geoObjects.add(myPlacemark2);
            
            placemark2 = 1;
            ymaps.geocode(myPlacemark2.geometry.getCoordinates(), {
                results: 1
            }).then(function (res) {
                    if(placemark2 == 1 && placemark1 == 1){
                        var new_cent_lat = (Number(myPlacemark.geometry.getCoordinates()[0]) + Number(myPlacemark2.geometry.getCoordinates()[0]))/2;
                        var new_cent_lon = (Number(myPlacemark.geometry.getCoordinates()[1]) + Number(myPlacemark2.geometry.getCoordinates()[1]))/2;
                        var new_center = [];
                        new_center.push(new_cent_lat);
                        new_center.push(new_cent_lon);
                        console.log(new_center);
                        myMap.setCenter(new_center, 12, {
    
                        });
                    }
                    else{
                        var firstGeoObject = res.geoObjects.get(0),
                            coords2 = firstGeoObject.geometry.getCoordinates(),
                            bounds = firstGeoObject.properties.get('boundedBy');
                        myMap.setCenter(coords2, 12, {
    
                        });
                    }
            });
            markers[1] = myPlacemark2;
            document.getElementById("latlongmet2").value = myPlacemark2.geometry.getCoordinates();
            show_adress_list2();
            if(placemark2 == 1 && placemark1 == 1){
                calcRoute();
            }
             //Отслеживаем событие перемещения метки
            myPlacemark2.events.add("dragend", function (e) {
                coords2 = this.geometry.getCoordinates();
                savecoordinats2();
                document.getElementById("message2").style.display='none';
                document.getElementById("latlongmet2").value = coords2;
                show_adress_list2();
                if(placemark2 == 1 && placemark1 == 1){
                    myMap.geoObjects.remove(route.getPaths());
                    reset();
                    calcRoute();
                }
            }, myPlacemark2);
            
            //Отслеживаем событие выбора результата поиска
//            SearchControl.events.add("resultselect", function (e) {
//                coords2 = SearchControl.getResultsArray()[0].geometry.getCoordinates();
//                savecoordinats2();
//                document.getElementById("latlongmet2").value = coords2;
//                show_adress_list2();
//            });
            
            $('#CalltaxiForm_SourceAddress').val("Аэропорт");                        
    });
    
    $('#vokzal1').click(function(){
        coords = vokzal_kord;
        if(typeof(myPlacemark) != "undefined"){
            myMap.geoObjects.remove(myPlacemark);
        }
         myPlacemark = new ymaps.Placemark(vokzal_kord,{hintContent: 'Откуда едем'},
            {
                iconLayout: 'default#image',
                iconImageHref: '../img/a.png',
                iconImageSize: [50, 65],
                iconImageOffset: [-25, -32],
                draggable: true
            }); 
            myMap.geoObjects.add(myPlacemark);
            
            placemark1 = 1;
            ymaps.geocode(myPlacemark.geometry.getCoordinates(), {
                results: 1
            }).then(function (res) {
                    if(placemark2 == 1 && placemark1 == 1){
                        var new_cent_lat = (Number(myPlacemark.geometry.getCoordinates()[0]) + Number(myPlacemark2.geometry.getCoordinates()[0]))/2;
                        var new_cent_lon = (Number(myPlacemark.geometry.getCoordinates()[1]) + Number(myPlacemark2.geometry.getCoordinates()[1]))/2;
                        var new_center = [];
                        new_center.push(new_cent_lat);
                        new_center.push(new_cent_lon);
                        console.log(new_center);
                        myMap.setCenter(new_center, 12, {
    
                        });
                    }
                    else{
                        var firstGeoObject = res.geoObjects.get(0),
                            coords = firstGeoObject.geometry.getCoordinates(),
                            bounds = firstGeoObject.properties.get('boundedBy');
                        myMap.setCenter(coords, 12, {
    
                        });
                    }
            });
            markers[0] = myPlacemark;
            document.getElementById("latlongmet").value = myPlacemark.geometry.getCoordinates();
            show_adress_list();
            if(placemark2 == 1 && placemark1 == 1){
                calcRoute();
            }
             //Отслеживаем событие перемещения метки
            myPlacemark.events.add("dragend", function (e) {
                coords = this.geometry.getCoordinates();
                savecoordinats();
                document.getElementById("message2").style.display='none';
                document.getElementById("latlongmet").value = coords;
                show_adress_list();
                if(placemark2 == 1 && placemark1 == 1){
                    myMap.geoObjects.remove(route.getPaths());
                    reset();
                    calcRoute();
                }
            }, myPlacemark);
            
            //Отслеживаем событие выбора результата поиска
//            SearchControl.events.add("resultselect", function (e) {
//                coords = SearchControl.getResultsArray()[0].geometry.getCoordinates();
//                savecoordinats();
//                document.getElementById("latlongmet").value = coords;
//                show_adress_list();
//            });

    });
    
    $('#vokzal2').click(function(){
        coords2 = vokzal_kord;
        if(typeof(myPlacemark2) != "undefined"){
            myMap.geoObjects.remove(myPlacemark2);
        }
           myPlacemark2 = new ymaps.Placemark(vokzal_kord,{hintContent: 'Куда едем'},
            {
                iconLayout: 'default#image',
                iconImageHref: '../img/b.png',
                iconImageSize: [50, 65],
                iconImageOffset: [-25, -32],
                draggable: true
            }); 
            myMap.geoObjects.add(myPlacemark2);
            
            placemark2 = 1;
            ymaps.geocode(myPlacemark2.geometry.getCoordinates(), {
                results: 1
            }).then(function (res) {
                    if(placemark2 == 1 && placemark1 == 1){
                        var new_cent_lat = (Number(myPlacemark.geometry.getCoordinates()[0]) + Number(myPlacemark2.geometry.getCoordinates()[0]))/2;
                        var new_cent_lon = (Number(myPlacemark.geometry.getCoordinates()[1]) + Number(myPlacemark2.geometry.getCoordinates()[1]))/2;
                        var new_center = [];
                        new_center.push(new_cent_lat);
                        new_center.push(new_cent_lon);
                        console.log(new_center);
                        myMap.setCenter(new_center, 12, {
    
                        });
                    }
                    else{
                        var firstGeoObject = res.geoObjects.get(0),
                            coords2 = firstGeoObject.geometry.getCoordinates(),
                            bounds = firstGeoObject.properties.get('boundedBy');
                        myMap.setCenter(coords2, 12, {
    
                        });
                    }
            });
            markers[1] = myPlacemark2;
            document.getElementById("latlongmet2").value = myPlacemark2.geometry.getCoordinates();
            show_adress_list2();
            if(placemark2 == 1 && placemark1 == 1){
                calcRoute();
            }
             //Отслеживаем событие перемещения метки
            myPlacemark2.events.add("dragend", function (e) {
                coords2 = this.geometry.getCoordinates();
                savecoordinats2();
                document.getElementById("message2").style.display='none';
                document.getElementById("latlongmet2").value = coords2;
                show_adress_list2();
                if(placemark2 == 1 && placemark1 == 1){
                    myMap.geoObjects.remove(route.getPaths());
                    reset();
                    calcRoute();
                }
            }, myPlacemark2);
            
            //Отслеживаем событие выбора результата поиска
//            SearchControl.events.add("resultselect", function (e) {
//                coords2 = SearchControl.getResultsArray()[0].geometry.getCoordinates();
//                savecoordinats2();
//                document.getElementById("latlongmet2").value = coords2;
//                show_adress_list2();
//            });  
    });
    
    
    $('#almaty11').click(function(){
        coords = almaty1;
        if(typeof(myPlacemark) != "undefined"){
            myMap.geoObjects.remove(myPlacemark);
        }
        myPlacemark = new ymaps.Placemark(almaty1,{hintContent: 'Откуда едем'},
            {
                iconLayout: 'default#image',
                iconImageHref: '../img/a.png',
                iconImageSize: [50, 65],
                iconImageOffset: [-25, -32],
                draggable: true
            }); 
            myMap.geoObjects.add(myPlacemark);
            
            placemark1 = 1;
            ymaps.geocode(myPlacemark.geometry.getCoordinates(), {
                results: 1
            }).then(function (res) {
                    if(placemark2 == 1 && placemark1 == 1){
                        var new_cent_lat = (Number(myPlacemark.geometry.getCoordinates()[0]) + Number(myPlacemark2.geometry.getCoordinates()[0]))/2;
                        var new_cent_lon = (Number(myPlacemark.geometry.getCoordinates()[1]) + Number(myPlacemark2.geometry.getCoordinates()[1]))/2;
                        var new_center = [];
                        new_center.push(new_cent_lat);
                        new_center.push(new_cent_lon);
                        console.log(new_center);
                        myMap.setCenter(new_center, 12, {
    
                        });
                    }
                    else{
                        var firstGeoObject = res.geoObjects.get(0),
                            coords = firstGeoObject.geometry.getCoordinates(),
                            bounds = firstGeoObject.properties.get('boundedBy');
                        myMap.setCenter(coords, 12, {
    
                        });
                    }
            });
            markers[0] = myPlacemark;
            document.getElementById("latlongmet").value = myPlacemark.geometry.getCoordinates();
            show_adress_list();
            if(placemark2 == 1 && placemark1 == 1){
                calcRoute();
            }
             //Отслеживаем событие перемещения метки
            myPlacemark.events.add("dragend", function (e) {
                coords = this.geometry.getCoordinates();
                savecoordinats();
                document.getElementById("message2").style.display='none';
                document.getElementById("latlongmet").value = coords;
                show_adress_list();
                if(placemark2 == 1 && placemark1 == 1){
                    myMap.geoObjects.remove(route.getPaths());
                    reset();
                    calcRoute();
                }
            }, myPlacemark);
            
            //Отслеживаем событие выбора результата поиска
            SearchControl.events.add("resultselect", function (e) {
                coords = SearchControl.getResultsArray()[0].geometry.getCoordinates();
                savecoordinats();
                document.getElementById("latlongmet").value = coords;
                show_adress_list();
            });
        $('#CalltaxiForm_OrderSourceAddress').val("ЖД Вокзал Алматы 1");
    });
    
    
    $('#almaty12').click(function(){
        coords = almaty2;
        if(typeof(myPlacemark) != "undefined"){
            myMap.geoObjects.remove(myPlacemark);
        }
         myPlacemark = new ymaps.Placemark(almaty2,{hintContent: 'Откуда едем'},
            {
                iconLayout: 'default#image',
                iconImageHref: '../img/a.png',
                iconImageSize: [50, 65],
                iconImageOffset: [-25, -32],
                draggable: true
            }); 
            myMap.geoObjects.add(myPlacemark);
            
            placemark1 = 1;
            ymaps.geocode(myPlacemark.geometry.getCoordinates(), {
                results: 1
            }).then(function (res) {
                    if(placemark2 == 1 && placemark1 == 1){
                        var new_cent_lat = (Number(myPlacemark.geometry.getCoordinates()[0]) + Number(myPlacemark2.geometry.getCoordinates()[0]))/2;
                        var new_cent_lon = (Number(myPlacemark.geometry.getCoordinates()[1]) + Number(myPlacemark2.geometry.getCoordinates()[1]))/2;
                        var new_center = [];
                        new_center.push(new_cent_lat);
                        new_center.push(new_cent_lon);
                        console.log(new_center);
                        myMap.setCenter(new_center, 12, {
    
                        });
                    }
                    else{
                        var firstGeoObject = res.geoObjects.get(0),
                            coords = firstGeoObject.geometry.getCoordinates(),
                            bounds = firstGeoObject.properties.get('boundedBy');
                        myMap.setCenter(coords, 12, {
    
                        });
                    }
            });
            markers[0] = myPlacemark;
            document.getElementById("latlongmet").value = myPlacemark.geometry.getCoordinates();
            show_adress_list();
            if(placemark2 == 1 && placemark1 == 1){
                calcRoute();
            }
             //Отслеживаем событие перемещения метки
            myPlacemark.events.add("dragend", function (e) {
                coords = this.geometry.getCoordinates();
                savecoordinats();
                document.getElementById("message2").style.display='none';
                document.getElementById("latlongmet").value = coords;
                show_adress_list();
                if(placemark2 == 1 && placemark1 == 1){
                    myMap.geoObjects.remove(route.getPaths());
                    reset();
                    calcRoute();
                }
            }, myPlacemark);
            
            //Отслеживаем событие выбора результата поиска
//            SearchControl.events.add("resultselect", function (e) {
//                coords = SearchControl.getResultsArray()[0].geometry.getCoordinates();
//                savecoordinats();
//                document.getElementById("latlongmet").value = coords;
//                show_adress_list();
//            });
        $('#CalltaxiForm_OrderSourceAddress').val("ЖД Вокзал Алматы 2");
    });
    
    
    $('#almaty21').click(function(){
        coords2 = almaty1;
        if(typeof(myPlacemark2) != "undefined"){
            myMap.geoObjects.remove(myPlacemark2);
        }
           myPlacemark2 = new ymaps.Placemark(almaty1,{hintContent: 'Куда едем'},
            {
                iconLayout: 'default#image',
                iconImageHref: '../img/b.png',
                iconImageSize: [50, 65],
                iconImageOffset: [-25, -32],
                draggable: true
            }); 
            myMap.geoObjects.add(myPlacemark2);
            
            placemark2 = 1;
            ymaps.geocode(myPlacemark2.geometry.getCoordinates(), {
                results: 1
            }).then(function (res) {
                    if(placemark2 == 1 && placemark1 == 1){
                        var new_cent_lat = (Number(myPlacemark.geometry.getCoordinates()[0]) + Number(myPlacemark2.geometry.getCoordinates()[0]))/2;
                        var new_cent_lon = (Number(myPlacemark.geometry.getCoordinates()[1]) + Number(myPlacemark2.geometry.getCoordinates()[1]))/2;
                        var new_center = [];
                        new_center.push(new_cent_lat);
                        new_center.push(new_cent_lon);
                        console.log(new_center);
                        myMap.setCenter(new_center, 12, {
    
                        });
                    }
                    else{
                        var firstGeoObject = res.geoObjects.get(0),
                            coords2 = firstGeoObject.geometry.getCoordinates(),
                            bounds = firstGeoObject.properties.get('boundedBy');
                        myMap.setCenter(coords2, 12, {
    
                        });
                    }
            });
            markers[1] = myPlacemark2;
            document.getElementById("latlongmet2").value = myPlacemark2.geometry.getCoordinates();
            show_adress_list2();
            if(placemark2 == 1 && placemark1 == 1){
                calcRoute();
            }
             //Отслеживаем событие перемещения метки
            myPlacemark2.events.add("dragend", function (e) {
                coords2 = this.geometry.getCoordinates();
                savecoordinats2();
                document.getElementById("message2").style.display='none';
                document.getElementById("latlongmet2").value = coords2;
                show_adress_list2();
                if(placemark2 == 1 && placemark1 == 1){
                    myMap.geoObjects.remove(route.getPaths());
                    reset();
                    calcRoute();
                }
            }, myPlacemark2);
            
            //Отслеживаем событие выбора результата поиска
//            SearchControl.events.add("resultselect", function (e) {
//                coords2 = SearchControl.getResultsArray()[0].geometry.getCoordinates();
//                savecoordinats2();
//                document.getElementById("latlongmet2").value = coords2;
//                show_adress_list2();
//            });
        $('#CalltaxiForm_SourceAddress').val("ЖД Вокзал Алматы 1");
    });
    
    
    $('#almaty22').click(function(){
        coords2 = almaty2;
        if(typeof(myPlacemark2) != "undefined"){
            myMap.geoObjects.remove(myPlacemark2);
        }
           myPlacemark2 = new ymaps.Placemark(almaty2,{hintContent: 'Куда едем'},
            {
                iconLayout: 'default#image',
                iconImageHref: '../img/b.png',
                iconImageSize: [50, 65],
                iconImageOffset: [-25, -32],
                draggable: true
            }); 
            myMap.geoObjects.add(myPlacemark2);
            
            placemark2 = 1;
            ymaps.geocode(myPlacemark2.geometry.getCoordinates(), {
                results: 1
            }).then(function (res) {
                    if(placemark2 == 1 && placemark1 == 1){
                        var new_cent_lat = (Number(myPlacemark.geometry.getCoordinates()[0]) + Number(myPlacemark2.geometry.getCoordinates()[0]))/2;
                        var new_cent_lon = (Number(myPlacemark.geometry.getCoordinates()[1]) + Number(myPlacemark2.geometry.getCoordinates()[1]))/2;
                        var new_center = [];
                        new_center.push(new_cent_lat);
                        new_center.push(new_cent_lon);
                        console.log(new_center);
                        myMap.setCenter(new_center, 12, {
    
                        });
                    }
                    else{
                        var firstGeoObject = res.geoObjects.get(0),
                            coords2 = firstGeoObject.geometry.getCoordinates(),
                            bounds = firstGeoObject.properties.get('boundedBy');
                        myMap.setCenter(coords2, 12, {
    
                        });
                    }
            });
            markers[1] = myPlacemark2;
            document.getElementById("latlongmet2").value = myPlacemark2.geometry.getCoordinates();
            show_adress_list2();
            if(placemark2 == 1 && placemark1 == 1){
                calcRoute();
            }
             //Отслеживаем событие перемещения метки
            myPlacemark2.events.add("dragend", function (e) {
                coords2 = this.geometry.getCoordinates();
                savecoordinats2();
                document.getElementById("message2").style.display='none';
                document.getElementById("latlongmet2").value = coords2;
                show_adress_list2();
                if(placemark2 == 1 && placemark1 == 1){
                    myMap.geoObjects.remove(route.getPaths());
                    reset();
                    calcRoute();
                }
            }, myPlacemark2);
            
            //Отслеживаем событие выбора результата поиска
//            SearchControl.events.add("resultselect", function (e) {
//                coords2 = SearchControl.getResultsArray()[0].geometry.getCoordinates();
//                savecoordinats2();
//                document.getElementById("latlongmet2").value = coords2;
//                show_adress_list2();
//            });  
        $('#CalltaxiForm_SourceAddress').val("ЖД Вокзал Алматы 2");
    });
    
    
    $('#del1').click(function(){
        if(typeof(myPlacemark) != "undefined"){
            if($('#CalltaxiForm_OrderSourceAddress').val() != ""){
                myMap.geoObjects.remove(myPlacemark);
                placemark1 = 0;
            }
            $('#CalltaxiForm_OrderSourceAddress').val("");
            $('#dlina_puti').html(" 0 км ");
            $('#vremya_puti').html(" 0 мин ");
            myMap.geoObjects.remove(route.getPaths());
            reset();
        }
    });
    
    $('#del2').click(function(){
        if(typeof(myPlacemark) != "undefined"){
            if($('#CalltaxiForm_SourceAddress').val() != ""){
                myMap.geoObjects.remove(myPlacemark2);
                placemark2 = 0;
            }
            $('#CalltaxiForm_SourceAddress').val("");
            $('#dlina_puti').html(" 0 км ");
            $('#vremya_puti').html(" 0 мин ");
            myMap.geoObjects.remove(route.getPaths());
            reset();
        }
    });
    
    $('#form_clear').click(function(){
        if($('#CalltaxiForm_OrderSourceAddress').val() != ""){
            myMap.geoObjects.remove(myPlacemark);
            placemark1 = 0;
        }
        if($('#CalltaxiForm_SourceAddress').val() != ""){
            myMap.geoObjects.remove(myPlacemark2);
            placemark2 = 0;
        }
        $('#CalltaxiForm_OrderSourceAddress, #CalltaxiForm_SourceAddress, #CalltaxiForm_OrderTime, #PHONE_TO_DIAL, #PASSENGER, #COMMENT').val("");
            $('#dlina_puti').html(" 0 км ");
            $('#vremya_puti').html(" 0 мин ");
            $('#unsuccess').css('display', 'none');
            $('#reg_from').css('border', 'none'); 
            $('#reg_to').css('border', 'none');  
            $('#region_name').css('border', 'none'); 
            $('#region_tel').css('border', 'none');
            if($('#region_time').length != 0){
                $('#region_time').css('border', 'none');
            }
            else{
                $('#CalltaxiForm_OrderTime').css('border', '2px solid #f4f4f4');
            }
            
            myMap.geoObjects.remove(route.getPaths());
            reset();
    })
    
    $('#CalltaxiForm_OrderSourceAddress').click(function(){
       if(placemark1 ==  0){
            coords = gorod;
            myPlacemark = new ymaps.Placemark(gorod,{hintContent: 'Откуда едем'},
            {
                iconLayout: 'default#image',
                iconImageHref: '../img/a.png',
                iconImageSize: [50, 65],
                iconImageOffset: [-25, -32],
                draggable: true
            }); 
            myMap.geoObjects.add(myPlacemark);
            
            placemark1 = 1;
            ymaps.geocode(myPlacemark.geometry.getCoordinates(), {
                results: 1
            }).then(function (res) {
                    if(placemark2 == 1 && placemark1 == 1){
                        var new_cent_lat = (Number(myPlacemark.geometry.getCoordinates()[0]) + Number(myPlacemark2.geometry.getCoordinates()[0]))/2;
                        var new_cent_lon = (Number(myPlacemark.geometry.getCoordinates()[1]) + Number(myPlacemark2.geometry.getCoordinates()[1]))/2;
                        var new_center = [];
                        new_center.push(new_cent_lat);
                        new_center.push(new_cent_lon);
                        console.log(new_center);
                        myMap.setCenter(new_center, 12, {
    
                        });
                    }
                    else{
                        var firstGeoObject = res.geoObjects.get(0),
                            coords = firstGeoObject.geometry.getCoordinates(),
                            bounds = firstGeoObject.properties.get('boundedBy');
                        myMap.setCenter(coords, 12, {
    
                        });
                    }
            });
            markers[0] = myPlacemark;
            document.getElementById("latlongmet").value = myPlacemark.geometry.getCoordinates();
            show_adress_list();
            if(placemark2 == 1 && placemark1 == 1){
                calcRoute();
            }
            //Отслеживаем событие перемещения метки
            myPlacemark.events.add("dragend", function (e) {
                coords = this.geometry.getCoordinates();
                savecoordinats();
                document.getElementById("message2").style.display='none';
                document.getElementById("latlongmet").value = coords;
                show_adress_list();
                if(placemark2 == 1 && placemark1 == 1){
                    myMap.geoObjects.remove(route.getPaths());
                    reset();
                    calcRoute();
                }
            }, myPlacemark);
            
            //Отслеживаем событие выбора результата поиска
//            SearchControl.events.add("resultselect", function (e) {
//                coords = SearchControl.getResultsArray()[0].geometry.getCoordinates();
//                savecoordinats();
//                document.getElementById("latlongmet").value = coords;
//                show_adress_list();
//            });     
       }
       else{
            ymaps.geocode(myPlacemark.geometry.getCoordinates(), {
                results: 1
            }).then(function (res) {
                    if(placemark2 == 1 && placemark1 == 1){
                        var new_cent_lat = (Number(myPlacemark.geometry.getCoordinates()[0]) + Number(myPlacemark2.geometry.getCoordinates()[0]))/2;
                        var new_cent_lon = (Number(myPlacemark.geometry.getCoordinates()[1]) + Number(myPlacemark2.geometry.getCoordinates()[1]))/2;
                        var new_center = [];
                        new_center.push(new_cent_lat);
                        new_center.push(new_cent_lon);
                        console.log(new_center);
                        myMap.setCenter(new_center, 12, {
    
                        });
                    }
                    else{
                        var firstGeoObject = res.geoObjects.get(0),
                            coords = firstGeoObject.geometry.getCoordinates(),
                            bounds = firstGeoObject.properties.get('boundedBy');
                        myMap.setCenter(coords, 12, {
    
                        });
                    }
            });
            markers[0] = myPlacemark;
            document.getElementById("latlongmet").value = myPlacemark.geometry.getCoordinates();
            show_adress_list();
            }
    })
    
    $('#CalltaxiForm_SourceAddress').click(function(){
       if(placemark2 ==  0){
            coords2 = gorod222;
            myPlacemark2 = new ymaps.Placemark(gorod222,{hintContent: 'Куда едем'},
            {
                iconLayout: 'default#image',
                iconImageHref: '../img/b.png',
                iconImageSize: [50, 65],
                iconImageOffset: [-25, -32],
                draggable: true
            }); 
            myMap.geoObjects.add(myPlacemark2);
            
            placemark2 = 1;
            ymaps.geocode(myPlacemark2.geometry.getCoordinates(), {
                results: 1
            }).then(function (res) {
                    if(placemark2 == 1 && placemark1 == 1){
                        var new_cent_lat = (Number(myPlacemark.geometry.getCoordinates()[0]) + Number(myPlacemark2.geometry.getCoordinates()[0]))/2;
                        var new_cent_lon = (Number(myPlacemark.geometry.getCoordinates()[1]) + Number(myPlacemark2.geometry.getCoordinates()[1]))/2;
                        var new_center = [];
                        new_center.push(new_cent_lat);
                        new_center.push(new_cent_lon);
                        console.log(new_center);
                        myMap.setCenter(new_center, 12, {
    
                        });
                    }
                    else{
                        var firstGeoObject = res.geoObjects.get(0),
                            coords2 = firstGeoObject.geometry.getCoordinates(),
                            bounds = firstGeoObject.properties.get('boundedBy');
                        myMap.setCenter(coords2, 12, {
    
                        });
                    }
            });
            markers[1] = myPlacemark2;
            document.getElementById("latlongmet2").value = myPlacemark2.geometry.getCoordinates();
            show_adress_list2();
            if(placemark2 == 1 && placemark1 == 1){
                calcRoute();
            }
             //Отслеживаем событие перемещения метки
            myPlacemark2.events.add("dragend", function (e) {
                coords2 = this.geometry.getCoordinates();
                savecoordinats2();
                document.getElementById("message2").style.display='none';
                document.getElementById("latlongmet2").value = coords2;
                show_adress_list2();
                if(placemark2 == 1 && placemark1 == 1){
                    myMap.geoObjects.remove(route.getPaths());
                    reset();
                    calcRoute();
                }
            }, myPlacemark2);
            
            //Отслеживаем событие выбора результата поиска
//            SearchControl.events.add("resultselect", function (e) {
//                coords2 = SearchControl.getResultsArray()[0].geometry.getCoordinates();
//                savecoordinats2();
//                document.getElementById("latlongmet2").value = coords2;
//                show_adress_list2();
//            });     
       }
       else{
            ymaps.geocode(myPlacemark2.geometry.getCoordinates(), {
                results: 1
            }).then(function (res) {
                    if(placemark2 == 1 && placemark1 == 1){
                        var new_cent_lat = (Number(myPlacemark.geometry.getCoordinates()[0]) + Number(myPlacemark2.geometry.getCoordinates()[0]))/2;
                        var new_cent_lon = (Number(myPlacemark.geometry.getCoordinates()[1]) + Number(myPlacemark2.geometry.getCoordinates()[1]))/2;
                        var new_center = [];
                        new_center.push(new_cent_lat);
                        new_center.push(new_cent_lon);
                        console.log(new_center);
                        myMap.setCenter(new_center, 12, {
    
                        });
                    }
                    else{
                        var firstGeoObject = res.geoObjects.get(0),
                            coords2 = firstGeoObject.geometry.getCoordinates(),
                            bounds = firstGeoObject.properties.get('boundedBy');
                        myMap.setCenter(coords2, 12, {
    
                        });
                    }
            });
            markers[1] = myPlacemark2;
            document.getElementById("latlongmet2").value = myPlacemark2.geometry.getCoordinates();
            show_adress_list2();
        }
    })
    
    
    $('.tochka1').click(function(){
       if(placemark1 ==  0){
            coords = gorod;
            myPlacemark = new ymaps.Placemark(gorod,{hintContent: 'Откуда едем'},
            {
                iconLayout: 'default#image',
                iconImageHref: '../img/a.png',
                iconImageSize: [50, 65],
                iconImageOffset: [-25, -32],
                draggable: true
            }); 
            myMap.geoObjects.add(myPlacemark);
            
            placemark1 = 1;
            ymaps.geocode(myPlacemark.geometry.getCoordinates(), {
                results: 1
            }).then(function (res) {
                    if(placemark2 == 1 && placemark1 == 1){
                        var new_cent_lat = (Number(myPlacemark.geometry.getCoordinates()[0]) + Number(myPlacemark2.geometry.getCoordinates()[0]))/2;
                        var new_cent_lon = (Number(myPlacemark.geometry.getCoordinates()[1]) + Number(myPlacemark2.geometry.getCoordinates()[1]))/2;
                        var new_center = [];
                        new_center.push(new_cent_lat);
                        new_center.push(new_cent_lon);
                        console.log(new_center);
                        myMap.setCenter(new_center, 12, {
    
                        });
                    }
                    else{
                        var firstGeoObject = res.geoObjects.get(0),
                            coords = firstGeoObject.geometry.getCoordinates(),
                            bounds = firstGeoObject.properties.get('boundedBy');
                        myMap.setCenter(coords, 12, {
    
                        });
                    }
            });
            markers[0] = myPlacemark;
            document.getElementById("latlongmet").value = myPlacemark.geometry.getCoordinates();
            show_adress_list();
            if(placemark2 == 1 && placemark1 == 1){
                calcRoute();
            }
             //Отслеживаем событие перемещения метки
            myPlacemark.events.add("dragend", function (e) {
                coords = this.geometry.getCoordinates();
                savecoordinats();
                document.getElementById("message2").style.display='none';
                document.getElementById("latlongmet").value = coords;
                show_adress_list();
                if(placemark2 == 1 && placemark1 == 1){
                    myMap.geoObjects.remove(route.getPaths());
                    reset();
                    calcRoute();
                }
            }, myPlacemark);
            
            //Отслеживаем событие выбора результата поиска
//            SearchControl.events.add("resultselect", function (e) {
//                coords = SearchControl.getResultsArray()[0].geometry.getCoordinates();
//                savecoordinats();
//                document.getElementById("latlongmet").value = coords;
//                show_adress_list();
//            });     
       }
       else{
            ymaps.geocode(myPlacemark.geometry.getCoordinates(), {
                results: 1
            }).then(function (res) {
                    if(placemark2 == 1 && placemark1 == 1){
                        var new_cent_lat = (Number(myPlacemark.geometry.getCoordinates()[0]) + Number(myPlacemark2.geometry.getCoordinates()[0]))/2;
                        var new_cent_lon = (Number(myPlacemark.geometry.getCoordinates()[1]) + Number(myPlacemark2.geometry.getCoordinates()[1]))/2;
                        var new_center = [];
                        new_center.push(new_cent_lat);
                        new_center.push(new_cent_lon);
                        console.log(new_center);
                        myMap.setCenter(new_center, 12, {
    
                        });
                    }
                    else{
                        var firstGeoObject = res.geoObjects.get(0),
                            coords = firstGeoObject.geometry.getCoordinates(),
                            bounds = firstGeoObject.properties.get('boundedBy');
                        myMap.setCenter(coords, 12, {
    
                        });
                    }
            });
            markers[0] = myPlacemark;
            document.getElementById("latlongmet").value = myPlacemark.geometry.getCoordinates();
            show_adress_list();
            }
    })
    
    $('.tochka2').click(function(){
       if(placemark2 ==  0){
            coords2 = gorod222;
            myPlacemark2 = new ymaps.Placemark(gorod222,{hintContent: 'Куда едем'},
            {
                iconLayout: 'default#image',
                iconImageHref: '../img/b.png',
                iconImageSize: [50, 65],
                iconImageOffset: [-25, -32],
                draggable: true
            }); 
            myMap.geoObjects.add(myPlacemark2);
            
            placemark2 = 1;
            ymaps.geocode(myPlacemark2.geometry.getCoordinates(), {
                results: 1
            }).then(function (res) {
                    if(placemark2 == 1 && placemark1 == 1){
                        var new_cent_lat = (Number(myPlacemark.geometry.getCoordinates()[0]) + Number(myPlacemark2.geometry.getCoordinates()[0]))/2;
                        var new_cent_lon = (Number(myPlacemark.geometry.getCoordinates()[1]) + Number(myPlacemark2.geometry.getCoordinates()[1]))/2;
                        var new_center = [];
                        new_center.push(new_cent_lat);
                        new_center.push(new_cent_lon);
                        console.log(new_center);
                        myMap.setCenter(new_center, 12, {
    
                        });
                    }
                    else{
                        var firstGeoObject = res.geoObjects.get(0),
                            coords2 = firstGeoObject.geometry.getCoordinates(),
                            bounds = firstGeoObject.properties.get('boundedBy');
                        myMap.setCenter(coords2, 12, {
    
                        });
                    }
            });
            markers[1] = myPlacemark2;
            document.getElementById("latlongmet2").value = myPlacemark2.geometry.getCoordinates();
            show_adress_list2();
            if(placemark2 == 1 && placemark1 == 1){
                calcRoute();
            }
             //Отслеживаем событие перемещения метки
            myPlacemark2.events.add("dragend", function (e) {
                coords2 = this.geometry.getCoordinates();
                savecoordinats2();
                document.getElementById("message2").style.display='none';
                document.getElementById("latlongmet2").value = coords2;
                show_adress_list2();
                if(placemark2 == 1 && placemark1 == 1){
                    myMap.geoObjects.remove(route.getPaths());
                    reset();
                    calcRoute();
                }
            }, myPlacemark2);
            
            //Отслеживаем событие выбора результата поиска
//            SearchControl.events.add("resultselect", function (e) {
//                coords2 = SearchControl.getResultsArray()[0].geometry.getCoordinates();
//                savecoordinats2();
//                document.getElementById("latlongmet2").value = coords2;
//                show_adress_list2();
//            });     
       }
       else{
            ymaps.geocode(myPlacemark2.geometry.getCoordinates(), {
                results: 1
            }).then(function (res) {
                    if(placemark2 == 1 && placemark1 == 1){
                        var new_cent_lat = (Number(myPlacemark.geometry.getCoordinates()[0]) + Number(myPlacemark2.geometry.getCoordinates()[0]))/2;
                        var new_cent_lon = (Number(myPlacemark.geometry.getCoordinates()[1]) + Number(myPlacemark2.geometry.getCoordinates()[1]))/2;
                        var new_center = [];
                        new_center.push(new_cent_lat);
                        new_center.push(new_cent_lon);
                        console.log(new_center);
                        myMap.setCenter(new_center, 12, {
    
                        });
                    }
                    else{
                        var firstGeoObject = res.geoObjects.get(0),
                            coords2 = firstGeoObject.geometry.getCoordinates(),
                            bounds = firstGeoObject.properties.get('boundedBy');
                        myMap.setCenter(coords2, 12, {
    
                        });
                    }
            });
            markers[1] = myPlacemark2;
            document.getElementById("latlongmet2").value = myPlacemark2.geometry.getCoordinates();
            show_adress_list2();
            }
    })
    
    //Отслеживаем событие щелчка по карте
    myMap.events.add('click', function (e) {
        if(placemark2 == 1 && placemark1 == 1){
            myMap.geoObjects.remove(route.getPaths());
            reset();
            calcRoute();
        }
    });
    
    myMap.events.add('boundschange', function (event) {
    if (event.get('newZoom') != event.get('oldZoom')) {  

        }
    });
    
    $('#CalltaxiForm_SourceAddress').keyup(function(event){
        event.preventDefault();
        document.onkeyup = function (e) {
    	    e = e || window.event;
    	    if (e.keyCode === 13) {
                var text = $('#CalltaxiForm_SourceAddress').val();
                //console.log(text_gorod + text);
                var myGeocoder = ymaps.geocode((text_gorod + text),{results: 1});
                myGeocoder.then(
                    function (res) {
                    myMap.geoObjects.add(res.geoObjects);
                    //console.log(res.geoObjects);
                    coords2 = res.geoObjects.getBounds()[0];
                    myMap.geoObjects.remove(res.geoObjects);
                    
                    if(typeof(myPlacemark2) != "undefined"){
                        myMap.geoObjects.remove(myPlacemark2);
                    };
                    myPlacemark2 = new ymaps.Placemark(coords2,{hintContent: 'Куда едем'},
                        {
                            iconLayout: 'default#image',
                            iconImageHref: '../img/b.png',
                            iconImageSize: [50, 65],
                            iconImageOffset: [-25, -32],
                            draggable: true
                        }); 
                        myMap.geoObjects.add(myPlacemark2);
                        
                        placemark2 = 1;
                        ymaps.geocode(myPlacemark2.geometry.getCoordinates(), {
                            results: 1
                        }).then(function (res) {
                            if(placemark2 == 1 && placemark1 == 1){
                                var new_cent_lat = (Number(myPlacemark.geometry.getCoordinates()[0]) + Number(myPlacemark2.geometry.getCoordinates()[0]))/2;
                                var new_cent_lon = (Number(myPlacemark.geometry.getCoordinates()[1]) + Number(myPlacemark2.geometry.getCoordinates()[1]))/2;
                                var new_center = [];
                                new_center.push(new_cent_lat);
                                new_center.push(new_cent_lon);
                                console.log(new_center);
                                myMap.setCenter(new_center, 12, {
            
                                });
                            }
                            else{
                                var firstGeoObject = res.geoObjects.get(0),
                                    coords2 = firstGeoObject.geometry.getCoordinates(),
                                    bounds = firstGeoObject.properties.get('boundedBy');
                                myMap.setCenter(coords2, 12, {
            
                                });
                            }
                        });
                        markers[1] = myPlacemark2;
                        document.getElementById("latlongmet2").value = myPlacemark2.geometry.getCoordinates();
                        show_adress_list2();
                        if(placemark2 == 1 && placemark1 == 1){
                            calcRoute();
                        }
        
                        myPlacemark2.events.add("dragend", function (e) {
                            coords2 = this.geometry.getCoordinates();
                            savecoordinats2();
                            document.getElementById("message2").style.display='none';
                            document.getElementById("latlongmet2").value = coords2;
                            show_adress_list2();
                            if(placemark2 == 1 && placemark1 == 1){
                                myMap.geoObjects.remove(route.getPaths());
                                reset();
                                calcRoute();
                            }
                        }, myPlacemark2);
                        
                        //Отслеживаем событие выбора результата поиска
//                        SearchControl.events.add("resultselect", function (e) {
//                            coords2 = SearchControl.getResultsArray()[0].geometry.getCoordinates();
//                            savecoordinats2();
//                            document.getElementById("latlongmet2").value = coords2;
//                            show_adress_list2();
//                        });  
        
                    },
                    function (err) {
                        // обработка ошибки
                    }
                );
    	    }
            return false;  
	   }
    })
    
    $('#CalltaxiForm_OrderSourceAddress').keyup(function(event){
        event.preventDefault();
        document.onkeyup = function (e) {
    	    e = e || window.event;
    	    if (e.keyCode === 13) {
                var text = $('#CalltaxiForm_OrderSourceAddress').val();
                //console.log(text_gorod + text);
                var myGeocoder = ymaps.geocode((text_gorod + text),{results: 1});
                myGeocoder.then(
                    function (res) {
                    myMap.geoObjects.add(res.geoObjects);
                    //console.log(res.geoObjects);
                    coords = res.geoObjects.getBounds()[0];
                    myMap.geoObjects.remove(res.geoObjects);
                    
                    if(typeof(myPlacemark) != "undefined"){
                        myMap.geoObjects.remove(myPlacemark);
                    };
                    myPlacemark = new ymaps.Placemark(coords,{hintContent: 'Откуда едем'},
                        {
                            iconLayout: 'default#image',
                            iconImageHref: '../img/a.png',
                            iconImageSize: [50, 65],
                            iconImageOffset: [-25, -32],
                            draggable: true
                        }); 
                        myMap.geoObjects.add(myPlacemark);
                        
                        placemark1 = 1;
                        ymaps.geocode(myPlacemark.geometry.getCoordinates(), {
                            results: 1
                        }).then(function (res) {
                            if(placemark2 == 1 && placemark1 == 1){
                                var new_cent_lat = (Number(myPlacemark.geometry.getCoordinates()[0]) + Number(myPlacemark2.geometry.getCoordinates()[0]))/2;
                                var new_cent_lon = (Number(myPlacemark.geometry.getCoordinates()[1]) + Number(myPlacemark2.geometry.getCoordinates()[1]))/2;
                                var new_center = [];
                                new_center.push(new_cent_lat);
                                new_center.push(new_cent_lon);
                                console.log(new_center);
                                myMap.setCenter(new_center, 12, {
            
                                });
                            }
                            else{
                                var firstGeoObject = res.geoObjects.get(0),
                                    coords = firstGeoObject.geometry.getCoordinates(),
                                    bounds = firstGeoObject.properties.get('boundedBy');
                                myMap.setCenter(coords, 12, {
            
                                });
                            }
                        });
                        markers[0] = myPlacemark;
                        document.getElementById("latlongmet").value = myPlacemark.geometry.getCoordinates();
                        show_adress_list();
                        if(placemark2 == 1 && placemark1 == 1){
                            calcRoute();
                        }
        
                        myPlacemark.events.add("dragend", function (e) {
                            coords = this.geometry.getCoordinates();
                            savecoordinats();
                            document.getElementById("message").style.display='none';
                            document.getElementById("latlongmet").value = coords;
                            show_adress_list();
                            if(placemark2 == 1 && placemark1 == 1){
                                myMap.geoObjects.remove(route.getPaths());
                                reset();
                                calcRoute();
                            }
                        }, myPlacemark);
                        
                        //Отслеживаем событие выбора результата поиска
//                        SearchControl.events.add("resultselect", function (e) {
//                            coords = SearchControl.getResultsArray()[0].geometry.getCoordinates();
//                            savecoordinats();
//                            document.getElementById("latlongmet").value = coords;
//                            show_adress_list();
//                        });  
        
                    },
                    function (err) {
                        // обработка ошибки
                    }
                );
    	    }
            return false;  
	   }
    })
    
    $('#CalltaxiForm_SourceAddress').bind("focusout", function(event){
        var text = $('#CalltaxiForm_SourceAddress').val();
        //console.log(text);
        if(text == "" || text == "ЖД Вокзал Алматы 1" || text == "ЖД Вокзал Алматы 2" || text == "ЖД Вокзал Астана" || text == "Аэропорт"){
          
        }
        else{
                //console.log(text_gorod + text);
                var myGeocoder = ymaps.geocode((text_gorod + text),{results: 1});
                myGeocoder.then(
                    function (res) {
                    myMap.geoObjects.add(res.geoObjects);
                    //console.log(res.geoObjects);
                    coords2 = res.geoObjects.getBounds()[0];
                    myMap.geoObjects.remove(res.geoObjects);
                    
                    if(typeof(myPlacemark2) != "undefined"){
                        myMap.geoObjects.remove(myPlacemark2);
                    };
                    myPlacemark2 = new ymaps.Placemark(coords2,{hintContent: 'Куда едем'},
                        {
                            iconLayout: 'default#image',
                            iconImageHref: '../img/b.png',
                            iconImageSize: [50, 65],
                            iconImageOffset: [-25, -32],
                            draggable: true
                        }); 
                        myMap.geoObjects.add(myPlacemark2);
                        
                        placemark2 = 1;
                        ymaps.geocode(myPlacemark2.geometry.getCoordinates(), {
                            results: 1
                        }).then(function (res) {
                            var firstGeoObject = res.geoObjects.get(0),
                                coords3 = firstGeoObject.geometry.getCoordinates(),
                                bounds = firstGeoObject.properties.get('boundedBy');
                            myMap.setCenter(coords, 12, {
        
                            });
                        });
                        markers[1] = myPlacemark2;
                        document.getElementById("latlongmet2").value = myPlacemark2.geometry.getCoordinates();
                        show_adress_list2();
                        if(placemark2 == 1 && placemark1 == 1){
                            calcRoute();
                        }
        
                        myPlacemark2.events.add("dragend", function (e) {
                            coords2 = this.geometry.getCoordinates();
                            savecoordinats2();
                            document.getElementById("message2").style.display='none';
                            document.getElementById("latlongmet2").value = coords2;
                            show_adress_list2();
                            if(placemark2 == 1 && placemark1 == 1){
                                myMap.geoObjects.remove(route.getPaths());
                                reset();
                                calcRoute();
                            }
                        }, myPlacemark2);
                        
                        //Отслеживаем событие выбора результата поиска
//                        SearchControl.events.add("resultselect", function (e) {
//                            coords2 = SearchControl.getResultsArray()[0].geometry.getCoordinates();
//                            savecoordinats2();
//                            document.getElementById("latlongmet2").value = coords2;
//                            show_adress_list2();
//                        });  
        
                    },
                    function (err) {
                        // обработка ошибки
                    }
                );    
        }
    })
    
    $('#CalltaxiForm_OrderSourceAddress').bind("focusout", function(){
        var text = $('#CalltaxiForm_OrderSourceAddress').val();
        //console.log(text);
        if(text == "" || text == "ЖД Вокзал Алматы 1" || text == "ЖД Вокзал Алматы 2" || text == "ЖД Вокзал Астана" || text == "Аэропорт"){

        }
        else{
                //console.log(text_gorod + text);
                var myGeocoder = ymaps.geocode((text_gorod + text),{results: 1});
                myGeocoder.then(
                    function (res) {
                    myMap.geoObjects.add(res.geoObjects);
                    //console.log(res.geoObjects);
                    coords = res.geoObjects.getBounds()[0];
                    myMap.geoObjects.remove(res.geoObjects);
                    
                    if(typeof(myPlacemark) != "undefined"){
                        myMap.geoObjects.remove(myPlacemark);
                    };
                    myPlacemark = new ymaps.Placemark(coords,{hintContent: 'Откуда едем'},
                        {
                            iconLayout: 'default#image',
                            iconImageHref: '../img/a.png',
                            iconImageSize: [50, 65],
                            iconImageOffset: [-25, -32],
                            draggable: true
                        }); 
                        myMap.geoObjects.add(myPlacemark);
                        
                        placemark1 = 1;
                        ymaps.geocode(myPlacemark.geometry.getCoordinates(), {
                            results: 1
                        }).then(function (res) {
                            var firstGeoObject = res.geoObjects.get(0),
                                coords3 = firstGeoObject.geometry.getCoordinates(),
                                bounds = firstGeoObject.properties.get('boundedBy');
                                myMap.setCenter(coords, 12, {
            
                                });
                        });
                        markers[0] = myPlacemark;
                        document.getElementById("latlongmet").value = myPlacemark.geometry.getCoordinates();
                        show_adress_list();
                        if(placemark2 == 1 && placemark1 == 1){
                            calcRoute();
                        }
        
                        myPlacemark.events.add("dragend", function (e) {
                            coords = this.geometry.getCoordinates();
                            savecoordinats();
                            document.getElementById("message").style.display='none';
                            document.getElementById("latlongmet").value = coords;
                            show_adress_list();
                            if(placemark2 == 1 && placemark1 == 1){
                                myMap.geoObjects.remove(route.getPaths());
                                reset();
                                calcRoute();
                            }
                        }, myPlacemark);
                        
                        //Отслеживаем событие выбора результата поиска
//                        SearchControl.events.add("resultselect", function (e) {
//                            coords = SearchControl.getResultsArray()[0].geometry.getCoordinates();
//                            savecoordinats();
//                            document.getElementById("latlongmet").value = coords;
//                            show_adress_list();
//                        });  
        
                    },
                    function (err) {
                        // обработка ошибки
                    }
                );   
        }
    })
    
}
 
function calcRoute() {
    if(route != null){
        myMap.geoObjects.remove(route.getPaths());
        route = null;        
    }
        
    for(var i = 0, l = markers.length; i < l; i++) {
        points[i] = markers[i].geometry.getCoordinates();
    }
                
    $('#ballsWaveG').css('display', 'block');
    
    ymaps.route(points, {
        mapStateAutoApply: true
    }).then(function (router) {
    route = router;
    route.getPaths().options.set({
        strokeColor : 'F51800',
        strokeWidth : 4,
        opacity: 0.9
        
    });
    
    myMap.geoObjects.add(route.getPaths());
    get_info_crews();

    var routeLength = route.getLength(); // Длина маршрута
    var routeLength = route.getLength(); // Длина маршрута
    var firstPath = route.getPaths(); // Первый путь
    var firstPathLength = route.getLength(); // Длина первого пути
    var firtstPathTime = route.getTime(); // Время без учета пробок 
    var firstPathJamsTime = route.getJamsTime();// Время с пробками
                
    var dlina_m = Math.ceil(firstPathLength);            
    var gorod_name = $('#gorod_input').val();   

    dlina_m = (dlina_m % 100) >= 50 ? parseInt(dlina_m / 100) * 100 + 100 : parseInt(dlina_m / 100) * 100;
    dlina_m=dlina_m/1000;
				
//    console.log("dlina - " + dlina);
//    console.log("bez probok - " + bez_probok);
//    console.log("s_probkami - " + s_probkami);
//    console.log("cena - " + cena);
//    console.log("podacha_1 - " + podacha_1);
    var pp = $('#CalltaxiForm_OrderSourceAddress').val();
    var dd = $('#CalltaxiForm_SourceAddress').val();
    if(pp.length > 3 && dd.length > 3){
        $('#dlina_puti').html(" " + dlina_m + " км ");
        $('#vremya_puti').html(" " + Math.ceil(firstPathJamsTime/60) + " мин ");
        
    var gorod_text;
    var tariff_id;
    var distance_city = dlina_m;
    var hourly_minutes = Math.ceil(firstPathJamsTime/60);
    
    var coc_city = getCookie('gorod_landing');
        gorod_text = 'almaty';
        
        tariff_id = 208;
        
        var address_source = $('#CalltaxiForm_OrderSourceAddress').val();
        var address_dest = $('#CalltaxiForm_SourceAddress').val();
        var lat1 = markers[0].geometry.getCoordinates()[0];
        var lon1 = markers[0].geometry.getCoordinates()[1];
        var lat2 = markers[1].geometry.getCoordinates()[0];
        var lon2 = markers[1].geometry.getCoordinates()[1];

        var url="summa_zakaza.php?gorod="+gorod_text+"&tariff_id="+tariff_id+"&distance_city="+distance_city+"&hourly_minutes="+hourly_minutes+"&source="+address_source+"&dest="+address_dest+"&lat1="+lat1+"&lat2="+lat2+"&lon1="+lon1+"&lon2="+lon2;
        $.ajax({  
        type: "GET",
        url: url,
        dataType: "html",
        cache: false,
        success: function(data) {
            var json = $.parseJSON(data);
            //console.log(json);
            var summa_zakaza = json['data']['sum'];
            var summa_so_skidkoi = summa_zakaza - summa_zakaza/10;
            $('#sedan_zach').html(summa_zakaza);
            $('#sedan_skidka').html(" " + summa_so_skidkoi + " т. ");
            }
        })


        tariff_id = 32;
        // hmm
        //[цена за км * расстояние (при скорости выше 20 км\ч)] +[ цена за мин * время (при скорости ниже 20 км\ч)], но не меньше минимальной стоимости.
        cena_za_km = 50;
        // 50 тенге за 2 км
        // от 300 тенге
        distance_city_price = distance_city  - 2;
    //*hourly_minutes ololo
        second_price = 500+ cena_za_km*distance_city_price;
        first_price = 300+ cena_za_km*distance_city_price;
        third_price = 700+ cena_za_km*distance_city_price;
        $('#cross_skidka').html(" " + second_price.toFixed(1)  + " т. ");
        $('#sedan_skidka').html(" " + first_price.toFixed(1)  + " т. ");
        //$('#vip_skidka').html(" " + third_price.toFixed(1)  + " т. ");
        $('#cross_zach').html((second_price+100).toFixed(0));
        $('#sedan_zach').html((first_price+200).toFixed(0));

        // endhmm
        
        var url="summa_zakaza.php?gorod="+gorod_text+"&tariff_id="+tariff_id+"&distance_city="+distance_city+"&hourly_minutes="+hourly_minutes+"&source="+address_source+"&dest="+address_dest+"&lat1="+lat1+"&lat2="+lat2+"&lon1="+lon1+"&lon2="+lon2;
        $.ajax({  
        type: "GET",
        url: url,
        dataType: "html",
        cache: false,
        success: function(data) {
            var json = $.parseJSON(data);
            var summa_zakaza = json['data']['sum'];
            var summa_so_skidkoi = summa_zakaza - summa_zakaza/10;
            $('#cross_zach').html(summa_zakaza);
            $('#cross_skidka').html(" " + summa_so_skidkoi + " т. ");
            }
        })
    
    }
    
    
    
    
    $('#ballsWaveG').css('display', 'none');            
    }, function (error) {
        alert("Возникла ошибка: " + error.message);
    });
}

//Удаление маршрута и меток с карты и очистка данных
function reset() {
    myMap.geoObjects.remove(route.getPaths());
    route = null;        
}


 
//Функция для передачи полученных значений в форму
function savecoordinats (){ 
    document.getElementById("latlongmet").value = myPlacemark.geometry.getCoordinates();
    markers[0] = myPlacemark;
}

function savecoordinats2 (){ 
    document.getElementById("latlongmet2").value = myPlacemark2.geometry.getCoordinates();
    markers[1] = myPlacemark2;
}

function sendMail(dataString)
{
    var url="sendMail.php?nomer_tel="+ dataString;
    $.ajax({  
        type: "GET",
        url: url,
        dataType: "html",
        cache: false,
        success: function(data) {
            //console.log(data);
            var succ_res = '<div class="overlay" title="окно" style="opacity: 1; visibility: visible;"></div><div id="parent_popup4">\
                           <div id="myblock4" class="myClass4">\
                        <span style="float: right; margin-right: 10px; margin-top: 10px; cursor: pointer"><a id="open-close4"><img style="width: 40px;;" src="img/close.png" /></a></span>\
                            <div style="width: 300px; margin: auto;">\
                                <p style="margin-top: 30px; margin-left: 40px; margin-bottom: 30px; text-align: center; font-size: 28px; color: #fff61a; font-weight: 500; line-height: 0;">Спасибо!</p>\
                                <p style="text-align: center; font-size: 18px; line-height: 0;">Ваш запрос был принят.</p>\
                                <p style="text-align: center; font-size: 18px; line-height: 1; color: #848685;">Мы перевзоням вам в течении<br /> 5 мин.</p>\
                            </div>\
                        </div>\
                        <div class="clear"></div>\
                    </div>';
            $('#for_email_success').html(succ_res);
            $('.myClass4').animate({'left':'40%'},550);
            $('#open-close4').click(function(event) {
            event.preventDefault();
                $('#myblock4').slideToggle();
                document.getElementById('parent_popup4').style.display='none';
                //alert("Ваш запрос успешно отпралено!\nВам перезвонять в течении 5 мин.")
                //$('#nishtyak').val("");
                $('.tell_z').val("");
                $('.name_z').val("");
            });
        }
    });  
}

function sendMail2(mmmm, dataString)
{
    var url="sendMail.php?mail="+mmmm+"&data="+ dataString;
    $.ajax({  
        type: "GET",
        url: url,
        dataType: "html",
        cache: false,
        success: function(data) {
            //console.log(data);
            $('#nomer_telefona').val("");
        }
    });  
}

function getCookie(name) {
  var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

function deleteCookie(name) {
  setCookie(name, "", { expires: -1 })
}

function add_coord(){
    var coord = document.getElementById("latlongmet").value;
    var new_comment = $('#COMMENT').val();
    if (coord == "") {
        $('#COMMENT').html(new_comment);
        alert(new_comment);
        $("calltaxi-form").submit();
    }else{
        var coord_parts = coord.split(',');
        var new_comment = new_comment + " ~" + coord_parts[0] + " " + coord_parts[1] + "~";
        $('#COMMENT').html(new_comment);
        alert(new_comment);
        $("calltaxi-form").submit();
    }            
}

function show_adress_list(){
    var coords11 = myPlacemark.geometry.getCoordinates();
    $.ajax({
        url: 'https://geocode-maps.yandex.ru/1.x/?geocode='+coords11+'&sco=latlong&kind=house&results=1',
        type: 'GET',
        dataType: 'html',
        success: function(xml){
            $("#message").html('');
            document.getElementById("show_list").style.display='none';
            jQuery(xml).find('featureMember').each(
                function()
                {   
                    	var qwerty = jQuery(this).find('name').text(),
        					coord = jQuery(this).find('pos').text(),
        					city = jQuery(this).find('AddressLine').text();
        				var address_parts = city.split(", ");
        				var coord_parts = coord.split(" ");  

//                        $('#message').prepend("<div class='addresses' onClick=\"insert_adress($(this).text(),'" + coord_parts[0] + "','" + coord_parts[1] + "','" + address_parts[0] + "','" + address_parts[1] + "','" + address_parts[2] + "')\" >" + qwerty + "</div>");
//                        console.log($(this).text());
//                        console.log(coord_parts[0]);
//                        console.log(coord_parts[1]);
//                        console.log(address_parts[0]);
//                        console.log(address_parts[1]);
//                        console.log(address_parts[2]);
                          //console.log(Number(myPlacemark.geometry.getCoordinates()[0]));
                          //console.log(Number(aeroport_kord[0]));
                          document.getElementById("CalltaxiForm_OrderSourceAddress").value = qwerty;
                          if((Number(myPlacemark.geometry.getCoordinates()[0]) == Number(aeroport_kord[0])) || (Number(myPlacemark.geometry.getCoordinates()[1]) == Number(aeroport_kord[1])) || (Number(myPlacemark.geometry.getCoordinates()[0]) == Math.round(Number(aeroport_kord[0])*10000)/10000) || (Number(myPlacemark.geometry.getCoordinates()[1]) == Math.round(Number(aeroport_kord[1])*10000)/10000)){
                                $('#CalltaxiForm_OrderSourceAddress').val("Аэропорт");
                          }
                          else if((Number(myPlacemark.geometry.getCoordinates()[0]) == Number(gorod[0])) || (Number(myPlacemark.geometry.getCoordinates()[1]) == Number(gorod[1])) || (Number(myPlacemark.geometry.getCoordinates()[0]) == Math.round(Number(gorod[0])*10000)/10000) || (Number(myPlacemark.geometry.getCoordinates()[1]) == Math.round(Number(gorod[1])*10000)/10000)){
                                $('#CalltaxiForm_OrderSourceAddress').val("");
                          }
                          //else if(typeof(almaty1) != "undefined"){
                              if((Number(myPlacemark.geometry.getCoordinates()[0]) == Number(almaty1[0])) || (Number(myPlacemark.geometry.getCoordinates()[1]) == Number(almaty1[1])) || (Number(myPlacemark.geometry.getCoordinates()[0]) == Math.round(Number(almaty1[0])*10000)/10000) || (Number(myPlacemark.geometry.getCoordinates()[1]) == Math.round(Number(almaty1[1])*10000)/10000)){
                                    $('#CalltaxiForm_OrderSourceAddress').val("ЖД Вокзал Алматы 1");
                              }
                              else if((Number(myPlacemark.geometry.getCoordinates()[0]) == Number(almaty2[0])) || (Number(myPlacemark.geometry.getCoordinates()[1]) == Number(almaty2[1])) || (Number(myPlacemark.geometry.getCoordinates()[0]) == Math.round(Number(almaty2[0])*10000)/10000) || (Number(myPlacemark.geometry.getCoordinates()[1]) == Math.round(Number(almaty2[1])*10000)/10000)){
                                    $('#CalltaxiForm_OrderSourceAddress').val("ЖД Вокзал Алматы 2");
                              }
                          //}
                          //else{
                            
                          //}
                });

           // document.getElementById("message").style.display='block'; 
        }
    });
}


function show_adress_list2(){
    var coords22 = myPlacemark2.geometry.getCoordinates();
    $.ajax({
        url: 'https://geocode-maps.yandex.ru/1.x/?geocode='+coords22+'&sco=latlong&kind=house&results=1',
        type: 'GET',
        dataType: 'html',
        success: function(xml){
            $("#message2").html('');
            document.getElementById("show_list2").style.display='none';
            jQuery(xml).find('featureMember').each(
                function()
                {   
                    	var qwerty = jQuery(this).find('name').text(),
        					coord = jQuery(this).find('pos').text(),
        					city = jQuery(this).find('AddressLine').text();
        				var address_parts = city.split(", ");
        				var coord_parts = coord.split(" ");  
                        
                        
//                        $('#message2').prepend("<div class='addresses2' onClick=\"insert_adress2($(this).text(),'" + coord_parts[0] + "','" + coord_parts[1] + "','" + address_parts[0] + "','" + address_parts[1] + "','" + address_parts[2] + "')\" >" + qwerty + "</div>");                      
//                        console.log("111" +coord_parts[0]);
//                        console.log("222" +coord_parts[1]);
//                        console.log("333" +address_parts[0]);
//                        console.log("444" +address_parts[1]);
//                        console.log("555" +address_parts[2]);
                          document.getElementById("CalltaxiForm_SourceAddress").value = qwerty;
                          if((Number(myPlacemark2.geometry.getCoordinates()[0]) == Number(aeroport_kord[0])) || (Number(myPlacemark2.geometry.getCoordinates()[1]) == Number(aeroport_kord[1])) || (Number(myPlacemark2.geometry.getCoordinates()[0]) == Math.round(Number(aeroport_kord[0])*10000)/10000) || (Number(myPlacemark2.geometry.getCoordinates()[1]) == Math.round(Number(aeroport_kord[1])*10000)/10000)){
                                $('#CalltaxiForm_SourceAddress').val("Аэропорт");
                          }
                          else if((Number(myPlacemark2.geometry.getCoordinates()[0]) == Number(gorod222[0])) || (Number(myPlacemark2.geometry.getCoordinates()[1]) == Number(gorod222[1])) || (Number(myPlacemark2.geometry.getCoordinates()[0]) == Math.round(Number(gorod222[0])*10000)/10000) || (Number(myPlacemark2.geometry.getCoordinates()[1]) == Math.round(Number(gorod222[1])*10000)/10000)){
                                $('#CalltaxiForm_SourceAddress').val("");
                          }
                          else if((Number(myPlacemark2.geometry.getCoordinates()[0]) == Number(vokzal_kord[0])) || (Number(myPlacemark2.geometry.getCoordinates()[1]) == Number(vokzal_kord[1])) || (Number(myPlacemark2.geometry.getCoordinates()[0]) == Math.round(Number(vokzal_kord[0])*10000)/10000) || (Number(myPlacemark2.geometry.getCoordinates()[1]) == Math.round(Number(vokzal_kord[1])*10000)/10000)){
                                $('#CalltaxiForm_SourceAddress').val("ЖД Вокзал Алматы 1");
                          }
                          else if(typeof(almaty1) != "undefined"){
                              if((Number(myPlacemark2.geometry.getCoordinates()[0]) == Number(almaty1[0])) || (Number(myPlacemark2.geometry.getCoordinates()[1]) == Number(almaty1[1])) || (Number(myPlacemark2.geometry.getCoordinates()[0]) == Math.round(Number(almaty1[0])*10000)/10000) || (Number(myPlacemark2.geometry.getCoordinates()[1]) == Math.round(Number(almaty1[1])*10000)/10000)){
                                    $('#CalltaxiForm_SourceAddress').val("ЖД Вокзал Алматы 1");
                              }
                              else if((Number(myPlacemark2.geometry.getCoordinates()[0]) == Number(almaty2[0])) || (Number(myPlacemark2.geometry.getCoordinates()[1]) == Number(almaty2[1])) || (Number(myPlacemark2.geometry.getCoordinates()[0]) == Math.round(Number(almaty2[0])*10000)/10000) || (Number(myPlacemark2.geometry.getCoordinates()[1]) == Math.round(Number(almaty2[1])*10000)/10000)){
                                    $('#CalltaxiForm_SourceAddress').val("ЖД Вокзал Алматы 2");
                              } 
                          }
                          else{
                            
                          }
                });
//           document.getElementById("message2").style.display='block'; 
        }
    });
}

function remove_successs(){ 
    $(document).ready(function() {
    var interval = setInterval(change_height, 5000);


    function change_height() {
        $('.successs').toggle(); 
        clearInterval(interval);
    }
    });           
    
}

function class_mashiny(blog1, blog2, blog3){
    blog1.css('background', '#FFFFFF');
    blog1.children(".ceny").css('background', '#F4AE0A');
    blog1.children("h3, p").css('color', '#2D923F');
    
    blog2.css('background', 'rgb(244, 174, 10)');
    blog2.children(".ceny").css('background', 'rgb(244, 174, 10)');
    blog2.children("h3, p").css('color', '#FFFFFF');
    
    blog3.css('background', 'rgb(244, 174, 10)');
    blog3.children(".ceny").css('background', 'rgb(244, 174, 10)');
    blog3.children("h3, p").css('color', '#FFFFFF');
}

function animateDiv(blog1, blog2, blog3, blog4){
    blog2.hide();
    blog3.hide();
    blog4.hide();
    blog1.show();
    $('body,html').animate({scrollTop: 0}, 100);
    //blog1.show("slide", { direction: "left" }, 700);
}

function change_gorod_menu(gorod_name) {
    $('.changer2').css('background', '#2E9441');
    if (gorod_name != 'Астана') {
        
		$('.changer2').animate({
			left: 100
		});
        $('#gorod11').css('color','white');
        $('#gorod22').css('color','#BCBEC0');
        $('.changer2').css('border-bottom-right-radius','12px');
        $('.changer2').css('border-top-right-radius','12px');
        $('.changer2').css('border-bottom-left-radius','0');
        $('.changer2').css('border-top-left-radius','0');
        document.cookie = "gorod_landing=almaty";
        $('.telefon_nomer_dlya_zv1').html("+7 (727) 3 900 500");
        $('.telefon_nomer_dlya_zv2').html("+7 (727) 672 77 77");
	}
    setTimeout("location.reload();", 500);
}


function setCookie(name, value, options) {
  options = options || {};

  var expires = options.expires;

  if (typeof expires == "number" && expires) {
    var d = new Date();
    d.setTime(d.getTime() + expires*1000);
    expires = options.expires = d;
  }
  if (expires && expires.toUTCString) { 
  	options.expires = expires.toUTCString();
  }

  value = encodeURIComponent(value);

  var updatedCookie = name + "=" + value;

  for(var propName in options) {
    updatedCookie += "; " + propName;
    var propValue = options[propName];    
    if (propValue !== true) { 
      updatedCookie += "=" + propValue;
     }
  }

  document.cookie = updatedCookie;
}


function change_time(stime) {
    if (stime == 1) {
        $('.changer_time').animate({
			left: 0
		});
        $('#time2').css('color','#CAD8CC');
        $('#time1').css('color','#414042');
        $('.changer_time').css('width','80px');
        $('.sel_time2').css('display', 'none');
        $('.sel_time').css('display', 'inline-block');
        vybor_time = 1;
	}  else {
		$('.changer_time').animate({
			left: 80
		});
        $('#time2').css('color','#414042');
        $('#time1').css('color','#CAD8CC');
        $('.changer_time').css('width','120px');
        $('.sel_time').css('display', 'none');
        $('.sel_time2').css('display', 'inline-block');
        vybor_time = 2;
	}
}

function select_time(stime) {
    if (stime == 1) {
        $('#minuty4').css('background','#f4f4f4');
        $('#minuty3').css('background','#f4f4f4');
        $('#minuty2').css('background','#f4f4f4');
        $('#minuty1').css('background','#feef00');
        $('#minuty1').css('color','#414042');
        $('#minuty2').css('color','#414042');
        $('#minuty3').css('color','#414042');
        $('#minuty4').css('color','#414042');
        vyb_pod = 'Сейчас';
	}  else if (stime == 2) {
        $('#minuty4').css('background','#f4f4f4');
        $('#minuty3').css('background','#f4f4f4');
        $('#minuty1').css('background','#f4f4f4');
        $('#minuty2').css('background','#feef00');
        $('#minuty2').css('color','#414042');
        $('#minuty1').css('color','#414042');
        $('#minuty3').css('color','#414042');
        $('#minuty4').css('color','#414042');
        vyb_pod = 'pl15';
	}  else if (stime == 3) {
        $('#minuty4').css('background','#f4f4f4');
        $('#minuty2').css('background','#f4f4f4');
        $('#minuty1').css('background','#f4f4f4');
        $('#minuty3').css('background','#feef00');
        $('#minuty3').css('color','#414042');
        $('#minuty1').css('color','#414042');
        $('#minuty2').css('color','#414042');
        $('#minuty4').css('color','#414042');
        vyb_pod = 'pl30';
	} else{
        $('#minuty1').css('background','#f4f4f4');
        $('#minuty3').css('background','#f4f4f4');
        $('#minuty2').css('background','#f4f4f4');
        $('#minuty4').css('background','#feef00');
        $('#minuty4').css('color','#414042');
        $('#minuty2').css('color','#414042');
        $('#minuty3').css('color','#414042');
        $('#minuty1').css('color','#414042');
        vyb_pod = 'pl45';
	}
}

function hide_map(){
    $('#yandex_map').slideToggle();
}

function hide_map2(){
    $('#yandex_map2').slideToggle();
}

function hide_map_current(){
    $('.for_map_current_order').css('display', 'none');
    $('#for_curr_table').css('display', 'inherit');
}

function zakaz_zvonka() {
    $('#loader-wrapper').css('display', 'inherit');
    var name = $('#call_name').val();
    var phone = $('#call_phone').val();

    var abonent = phone.substring(4, 7);
    var tri = phone.substring(9,12);
    var chetyre = phone.substring(13,19);
    phone = 8 + abonent + tri + chetyre;

    $.ajax({
        type: "POST",
        url: "call.php",
        data: {name:name,phone:phone},
        cache: false,
        success: function (data) {
            $('#loader-wrapper').delay(3000).css('display', 'none');
            var succ_res = '<div id="parent_popup"><div id="myblock" class="myClass"><span style="float: right; margin-right: 10px; margin-top: 10px; cursor: pointer"><a id="open-close"><img style="width: 40px;;" src="img/23.png" /></a></span><div style="width: 300px; margin: auto;"><p style="margin-top: 30px; margin-left: 40px; margin-bottom: 30px; text-align: center; font-size: 28px; color: #2D9131; font-weight: 500; line-height: 0;">Спасибо!</p><p style="text-align: center; font-size: 18px; line-height: 0;">Ваш заказ был принят.</p></div></div><div class="clear"></div></div>';
            $('.popup, .overlay').css('opacity','0');
            $('.popup, .overlay').css('visibility','hidden');
            $('body').prepend(succ_res);
            bbb();
        }
    });
}


function submit_order_form_client() {
    $('#loader-wrapper').css('display', 'inherit');
    var address_name = $('#address_name').val();
    var address_form = $('#CalltaxiForm_OrderSourceAddress').val();
        
    if(address_form != address_name){
        var new_address = address_form.replace(address_name, '');
        var address = address_name+''+new_address;
        $('#CalltaxiForm_OrderSourceAddress').val(address);
    }
		
	var address_source = $('#CalltaxiForm_OrderSourceAddress').val();//Адрес подачи
    var address_dest = $('#CalltaxiForm_SourceAddress').val();//Адрес назначения
	var phone = $('#PHONE_TO_DIAL').val();//телефон для отзвона
    var abonent = phone.substring(4, 7);
    var tri = phone.substring(9,12);
    var chetyre = phone.substring(13,19);
    phone = 8 + abonent + tri + chetyre;
    //console.log(phone);
    date_time = $('#CalltaxiForm_OrderTime').val();//время назначения
	var comment = $('#COMMENT').val();
    var passenger = $('#PASSENGER').val(); //Пассажир
	var coord = document.getElementById("latlongmet").value;//координаты
    var notice_start_sms,notice_start_call,notice_end_sms,notice_end_call;//уведомления
    var sposob_oplaty = "";
   sposob_oplaty = $('#sposoby_opl').val();//способ оплаты

    comment = comment + " " + sposob_oplaty;
    notice_start_sms = 0;
    notice_start_call = 1;
    notice_end_sms = 1;
    notice_end_call = 0;
    var notice = notice_start_sms+''+notice_start_call+'0'+notice_end_sms+''+notice_end_call+'0';
    if($('#CalltaxiForm_OrderSourceAddress').val() != ""){
        var lat1 = myPlacemark.geometry.getCoordinates()[0];
        var lon1 = myPlacemark.geometry.getCoordinates()[1];
        console.log(lat1 + ", " + lon1);
    }
    if($('#CalltaxiForm_SourceAddress').val() != ""){
        var lat2 = myPlacemark2.geometry.getCoordinates()[0];
        var lon2 = myPlacemark2.geometry.getCoordinates()[1];
        console.log(lat2 + ", " + lon2);
    } 
    var personal_area = 0;
    if($('#time1').length == 0){
        vybor_time = 2;
        personal_area = 1;
    }
    //console.log("lk"+personal_area);
    if(vybor_time == 2){
       	if (date_time == "Сейчас") {
    	   var d = new Date();
    	   var month = d.getMonth() + 1;
    	   var day = d.getDate();
    	   var hours = d.getHours();
    	   var minutes = d.getMinutes() + 15;
    	   if (hours < 10) {
    				hours = '0' + hours;
    	   }
    	   if (minutes > 60) {
    	       hours = hours + 1;
               minutes = minutes - 60;
    	   }
    	   if (minutes < 10) {
    	       minutes = '0' + minutes;
    	   }
    	   var output = d.getFullYear() + '.' + (('' + month).length < 2 ? '0' : '') + month + '.' + (('' + day).length < 2 ? '0' : '') + day + ' ' + hours + ':' + minutes;
    	   date_time = output;
	   }
       else{
           date_time = $('#CalltaxiForm_OrderTime').val();//время назначения
       }
    }
    else if(vybor_time == 1){
	   if (vyb_pod == "Сейчас") {
    	   var d = new Date();
    	   var month = d.getMonth() + 1;
    	   var day = d.getDate();
    	   var hours = d.getHours();
    	   var minutes = d.getMinutes() + 15;
    	   if (hours < 10) {
    				hours = '0' + hours;
    	   }
    	   if (minutes > 60) {
    	       hours = hours + 1;
               minutes = minutes - 60;
    	   }
    	   if (minutes < 10) {
    	       minutes = '0' + minutes;
    	   }
    	   var output = d.getFullYear() + '.' + (('' + month).length < 2 ? '0' : '') + month + '.' + (('' + day).length < 2 ? '0' : '') + day + ' ' + hours + ':' + minutes;
    	   date_time = output;
	   }
   	   else if (vyb_pod == "pl15") {
    	   var d = new Date();
    	   var month = d.getMonth() + 1;
    	   var day = d.getDate();
    	   var hours = d.getHours();
    	   var minutes = d.getMinutes() + 15;
    	   if (hours < 10) {
                hours = '0' + hours;
    	   }
    	   if (minutes > 60) {
    	       hours = hours + 1;
               minutes = minutes - 60;
    	   }
    	   if (minutes < 10) {
    	       minutes = '0' + minutes;
    	   }
    	   var output = d.getFullYear() + '.' + (('' + month).length < 2 ? '0' : '') + month + '.' + (('' + day).length < 2 ? '0' : '') + day + ' ' + hours + ':' + minutes;
    	   date_time = output;
	   }
       else if (vyb_pod == 'pl30') {
    	   var d = new Date();
    	   var month = d.getMonth() + 1;
    	   var day = d.getDate();
    	   var hours = d.getHours();
    	   var minutes = d.getMinutes() + 30;
    	   if (hours < 10) {
    				hours = '0' + hours;
    	   }
    	   if (minutes > 60) {
    	       hours = hours + 1;
               minutes = minutes - 60;
    	   }
    	   if (minutes < 10) {
    	       minutes = '0' + minutes;
    	   }
    	   var output = d.getFullYear() + '.' + (('' + month).length < 2 ? '0' : '') + month + '.' + (('' + day).length < 2 ? '0' : '') + day + ' ' + hours + ':' + minutes;
    	   date_time = output;
	   }
       else if (vyb_pod == 'pl45') {
    	   var d = new Date();
    	   var month = d.getMonth() + 1;
    	   var day = d.getDate();
    	   var hours = d.getHours();
    	   var minutes = d.getMinutes() + 45;
    	   if (hours < 10) {
    				hours = '0' + hours;
    	   }
    	   if (minutes > 60) {
    	       hours = hours + 1;
               minutes = minutes - 60;
    	   }
    	   if (minutes < 10) {
    	       minutes = '0' + minutes;
    	   }
    	   var output = d.getFullYear() + '.' + (('' + month).length < 2 ? '0' : '') + month + '.' + (('' + day).length < 2 ? '0' : '') + day + ' ' + hours + ':' + minutes;
    	   date_time = output;
	   }
    }
    var price='';
    var crew_group_id = 27;
    if(cls_mashiny == 'sedan'){
        crew_group_id = 'Toyota Corolla';
        price = $('#sedan_skidka').text();
    }
    else if(cls_mashiny == 'crossover'){
        crew_group_id = 'Toyota Camry';
        price = $('#cross_skidka').text();
    }
    else if(cls_mashiny == 'vip'){
        crew_group_id = 'Infinity Q 56';
        price = $('#vip_skidka').text();
    }
    //console.log(crew_group_id);
    //var url = "order-taxi-data.php?address_source=" + encodeURIComponent(address_source) + "&address_dest=" + encodeURIComponent(address_dest) + "&date_time=" + encodeURIComponent(date_time) + "&phone_to_dial=" + encodeURIComponent(phone) + "&comment=" + encodeURIComponent(comment) + "&passenger=" + encodeURIComponent(passenger) + "&crew_group_id=" + encodeURIComponent(crew_group_id) + "&notice=" + encodeURIComponent(notice) + "&lat1="+encodeURIComponent(myPlacemark.geometry.getCoordinates()[0]) + "&lon1="+encodeURIComponent(myPlacemark.geometry.getCoordinates()[1]) + "&lat2="+encodeURIComponent(myPlacemark2.geometry.getCoordinates()[0]) +"&lon2="+encodeURIComponent(myPlacemark2.geometry.getCoordinates()[1])+"&personal_area="+encodeURIComponent(personal_area);
    //console.log(myPlacemark.geometry.getCoordinates());
    //console.log(url);

    try{
        var url = "order-taxi-data.php?address_source=" + encodeURIComponent(address_source) + "&address_dest=" + encodeURIComponent(address_dest) + "&date_time=" + encodeURIComponent(date_time) + "&phone_to_dial=" + encodeURIComponent(phone) + "&comment=" + encodeURIComponent(comment) + "&passenger=" + encodeURIComponent(passenger) + "&crew_group_id=" + encodeURIComponent(crew_group_id) + "&notice=" + encodeURIComponent(notice) + "&lat1="+encodeURIComponent(myPlacemark.geometry.getCoordinates()[0]) + "&lon1="+encodeURIComponent(myPlacemark.geometry.getCoordinates()[1]) + "&lat2="+encodeURIComponent(myPlacemark2.geometry.getCoordinates()[0]) +"&lon2="+encodeURIComponent(myPlacemark2.geometry.getCoordinates()[1])+"&personal_area="+encodeURIComponent(personal_area);
        //console.log(myPlacemark.geometry.getCoordinates());
    }
    catch(e){
        var url = "order-taxi-data.php?address_source=" + encodeURIComponent(address_source) + "&address_dest=" + encodeURIComponent(address_dest) + "&date_time=" + encodeURIComponent(date_time) + "&phone_to_dial=" + encodeURIComponent(phone) + "&comment=" + encodeURIComponent(comment) + "&passenger=" + encodeURIComponent(passenger) + "&crew_group_id=" + encodeURIComponent(crew_group_id) + "&notice=" + encodeURIComponent(notice) + "&personal_area="+encodeURIComponent(personal_area);
    }
    // address_source  откуда
    //address_dest куда
    // phone
    //comment
    // passenger имя
    // date_time
    // crew_group_id машина
    // price

    $.ajax({
        type: "POST",
        url: "sender.php",
        data: {date_time:date_time,address_source:address_source,address_dest:address_dest,price:price,crew_group_id:crew_group_id,passenger:passenger,phone:phone,comment:comment},
        cache: false,
        success: function (data) {
            $('#loader-wrapper').delay(3000).css('display', 'none');
            var succ_res = '<div id="parent_popup"><div id="myblock" class="myClass"><span style="float: right; margin-right: 10px; margin-top: 10px; cursor: pointer"><a id="open-close"><img style="width: 40px;;" src="img/23.png" /></a></span><div style="width: 300px; margin: auto;"><p style="margin-top: 30px; margin-left: 40px; margin-bottom: 30px; text-align: center; font-size: 28px; color: #2D9131; font-weight: 500; line-height: 0;">Спасибо!</p><p style="text-align: center; font-size: 18px; line-height: 0;">Ваш заказ был принят.</p></div></div><div class="clear"></div></div>';
            $('body').prepend(succ_res);
            bbb();
        }
    });
    /*
    $.ajax({
        type: "GET",
        url: url,
        dataType: "html",
        cache: false,
        success: function(data) {
        var json = $.parseJSON(data);
        //console.log(json);
        $('#loader-wrapper').css('display', 'none');
        if (json.status == 'complete') {
            $('#unsuccess').css('display', 'inherit');
            $('#unsuccess').html('');
            $( ".success" ).remove();   
            $('.zakaz_input').val('');
            var succ_res = '<div id="parent_popup">\
                        <div id="myblock" class="myClass">\
                        <span style="float: right; margin-right: 10px; margin-top: 10px; cursor: pointer"><a id="open-close"><img style="width: 40px;;" src="img/close.png" /></a></span>\
                            <div style="width: 300px; margin: auto;">\
                                <p style="margin-top: 30px; margin-left: 40px; margin-bottom: 30px; text-align: center; font-size: 28px; color: #2D9131; font-weight: 500; line-height: 0;">Спасибо!</p>\
                                <p style="text-align: center; font-size: 18px; line-height: 0;">Ваш заказ был принят.</p>\
                                <p style="text-align: center; font-size: 24px; color: #2D9131;">Номер вашего заказа:</p>\
                                <p id="nomer_zakaza_f" style="font-size: 40px; text-align: center; line-height: 0; font-weight: 600;">' + json.order_id + '</p>\
                                <p style="text-align: center; font-size: 18px; line-height: 1; color: #848685;">Вы можете сохранить номер вашего заказа на своей почте.</p>\
                                <input id="nomer_telefona" type="text" placeholder="Введите ваш e-mail" style="color: #414042; width: 200px; height: 45px; font-size: 18px; border-radius: 10px; margin: 0 50px; border: 2px solid #2D9133; text-align: center;" /><br />\
                                <input id="sohranit_zakaz" type="button" name="sohranit_zakaz" value="СОХРАНИТЬ" style="background-color: #2D9133; cursor: pointer; color: #FFFFFF; width: 200px; height: 50px; font-size: 20px; border-radius: 10px; margin: 10px 50px 20px 50px;"/>\
                            </div>\
                        </div>\
                        <div class="clear"></div>\
                    </div>';
            $('#form').prepend(succ_res);
            $('#unsuccess').css('background', 'rgb(244, 174, 10)');
            bbb();    				    
        } else if (json.text != null) {
            $('#unsuccess').css('display', 'inherit');
            $('#unsuccess').html('');
            var unsucc_res = '<div id="parent_popup2">\
                        <div id="myblock2" class="myClass2">\
                        <span style="float: right; margin-right: 10px; margin-top: 10px; cursor: pointer"><a id="open-close2"><img style="width: 40px;;" src="img/close.png" /></a></span>\
                            <div style="width: 300px; margin: auto;">\
                                <p style="margin-top: 30px; margin-left: 40px; margin-bottom: 30px; text-align: center; font-size: 28px; color: #2D9131; font-weight: 500; line-height: 0;">Внимание!</p>\
                                <p style="text-align: center; font-size: 18px; line-height: 1; color: #848685;">Заказ не создан по техническим причинам. Приносим свои извинения.</p>\
                           </div>\
                        </div>\
                        <div class="clear"></div>\
                    </div>';
            $('#unsuccess').html(unsucc_res);
            $('#unsuccess').css('background', 'rgb(244, 174, 10)');
            bbb1();
        } else if (json.error_field != null) {
            $('#unsuccess').css('display', 'inherit');
            $('#un_right').html('');
            var text_error = '';
            var ii = 0;
            $.each(json.error_field, function() {
            if(ii == 0){
                $('#un_right').append("<li style='visibility:hidden'></li>");
            }
        $('#un_right').append("<li style='display: block; color: #FF0000; font-weight: 600;'>" + this + "</li>");
        if($('#CalltaxiForm_OrderSourceAddress').val() == ""){
            $('#reg_from').css('border', '2px solid red');                        
        }
        if($('#CalltaxiForm_SourceAddress').val() == ""){
            $('#reg_to').css('border', '2px solid red');                        
        }
        if($('#PASSENGER').val() == ""){
            $('#region_name').css('border', '2px solid red');                        
        }
        if($('#PHONE_TO_DIAL').val() == ""){
            $('#region_tel').css('border', '2px solid red');                        
        }
        if($('#CalltaxiForm_OrderTime').val() == ""){
            if($('#region_time').length != 0){
                $('#region_time').css('border', '2px solid red');
            }
            else{
                $('#CalltaxiForm_OrderTime').css('border', '2px solid red');
            }                 
        }
        
        $('body,html').animate({scrollTop: 0}, 100);
        ii++;
		      });
	       }
        }
    });*/
}

function PathRecip() {
    var OrderTime = $('#CalltaxiForm_OrderTime').val();
    if(OrderTime.substr(OrderTime.length-1) != '/') {
        $('#CalltaxiForm_SourceTime').attr("value",OrderTime.substr(0,4)+OrderTime.substr(5,2)+OrderTime.substr(8,2)+OrderTime.substr(11,2)+OrderTime.substr(14,2)+OrderTime.substr(17,2));
    }
}

function bbb(){
    $('#yandex_map').hide();
    //$('.myClass').animate({'left':'40%'},550);
    $('#open-close').click(function(event) {
        event.preventDefault();
        $('#yandex_map').show();
        $('#myblock').slideToggle();
        document.getElementById('parent_popup').style.display='none';
        $('#CalltaxiForm_OrderSourceAddress, #CalltaxiForm_SourceAddress, #CalltaxiForm_OrderTime, #PHONE_TO_DIAL, #PASSENGER, #COMMENT').val("");
        $('#unsuccess').css('display', 'none');
        $('.popup, .overlay').css('opacity','0');
        $('.popup, .overlay').css('visibility','hidden');
        $('#reg_from').css('border', 'none'); 

        $('#reg_to').css('border', 'none');  
        $('#region_name').css('border', 'none'); 
        $('#region_tel').css('border', 'none'); 
        if($('#region_time').length != 0){
            $('#region_time').css('border', 'none');
        }
        else{
            $('#CalltaxiForm_OrderTime').css('border', '2px solid #f4f4f4');
        }
        myMap.geoObjects.remove(myPlacemark2);
        $('#CalltaxiForm_SourceAddress').val("");
        placemark2 = 0;
        myMap.geoObjects.remove(myPlacemark);
        $('#CalltaxiForm_OrderSourceAddress').val("");
        placemark1 = 0;
        $('#dlina_puti').html(" 0 км ");
        $('#vremya_puti').html(" 0 мин ");
        myMap.geoObjects.remove(route.getPaths());
        reset();
    });
    
    $('#sohranit_zakaz').click(function(event) {
        contact_nomer = $('#nomer_telefona').val();
        nomer_zakazata = $('#nomer_zakaza_f').html();
        event.preventDefault();
        $('#yandex_map').show();
        $('#myblock').slideToggle();
        document.getElementById('parent_popup').style.display='none';
        $('#CalltaxiForm_OrderSourceAddress, #CalltaxiForm_SourceAddress, #CalltaxiForm_OrderTime, #PHONE_TO_DIAL, #PASSENGER, #COMMENT').val("");
        $('#unsuccess').css('display', 'none');
        $('#reg_from').css('border', 'none'); 
        $('#reg_to').css('border', 'none');  
        $('#region_name').css('border', 'none'); 
        $('#region_tel').css('border', 'none'); 
        if($('#region_time').length != 0){
            $('#region_time').css('border', 'none');
        }
        else{
            $('#CalltaxiForm_OrderTime').css('border', '2px solid #f4f4f4');
        } 
        contack_message = "Ваш номер заказа + +" + nomer_zakazata;
        if(contact_nomer != ''){
            dataString = contack_message;
            sendMail2(contact_nomer, dataString);
            myMap.geoObjects.remove(myPlacemark2);
            $('#CalltaxiForm_SourceAddress').val("");
            placemark2 = 0;
            myMap.geoObjects.remove(myPlacemark);
            $('#CalltaxiForm_OrderSourceAddress').val("");
            placemark1 = 0;
            $('#dlina_puti').html(" 0 км ");
            $('#vremya_puti').html(" 0 мин ");
            myMap.geoObjects.remove(route.getPaths());
            reset();
        }
        else
        {
            //alert('Введите номер телефона');
        }
    });   
}

function bbb1(){    
        $('.myClass2').animate({'left':'40%'},550);
        $('#open-close2').click(function(event) {
        event.preventDefault();
        $('#myblock2').slideToggle();
        document.getElementById('parent_popup2').style.display='none';
        $('#unsuccess').css('display', 'none');
   });    
}

$(function() {
    $('#calltaxi-form').submit(function() {
        var address_name = $('#address_name').val();
        var address_form = $('#CalltaxiForm_OrderSourceAddress').val();
        if(address_form != address_name){
            var new_address = address_form.replace(address_name, '');
            var address = address_name+'*'+new_address;
            $('#CalltaxiForm_OrderSourceAddress').val(address);
        }
    });
});

function insert_adress (asd, coord1, coord2, city, street, house){
    //alert(result);
    //alert(asd);
    $('#qwerty12345').html('');
    document.getElementById("CalltaxiForm_OrderSourceAddress").value = asd;
    
    $('#address_name').val(asd);
    document.getElementById("message").style.display='none';
    //insert_adress_into_base(coord1, coord2, city, street, house);
    //$('#yandex_map').slideToggle();
    //return false;
}

function insert_adress2 (asd, coord1, coord2, city, street, house){
    //alert(result);
    //alert(asd);
    $('#qwerty123456').html('');
    document.getElementById("CalltaxiForm_SourceAddress").value = asd;
    document.getElementById("message2").style.display='none';
    //insert_adress_into_base(coord1, coord2, city, street, house);
    //$('#yandex_map2').slideToggle();return false;
}

function check_toggle(div){
    if(div == "map2"){
        tochka = 2;
        //$('#yandex_map').css("display",'none');
        //$('#yandex_map').show();
    }
    if (div == "map1") {
        tochka = 1;
        //$('#yandex_map2').css("display",'none');
  //      $('#yandex_map').show();
    }
}


function get_info_crews(){
    $('#crews').html('');
    var coc_city = getCookie('gorod_landing');
    var gorod;
        gorod = 'almaty'; 

    //console.log(gorod);
    $.ajax({
        url: 'get_info.php?gorod='+gorod,
        type: 'GET',
        dataType: 'JSON',
        success: function(data){
        var markersasd = [];
        var myCollection = new ymaps.GeoObjectCollection();
        var driver_time = [];
        sort_mass.length = 0;
        count_waiting = 0;
        last_crew_wait = 0;
        for(var is = 0; is < data['data']['crews_coords'].length; is++){
            if(data['data']['crews_coords'][is]['state_kind'] == 'waiting'){
                count_waiting++;
                last_crew_wait = data['data']['crews_coords'][is]['crew_id'];
            }
        }
        
        data['data']['crews_coords'].forEach(function(item, i, data) {
            var mas_res2=[];
            var state = item['state_kind'];
            if(state == 'waiting'){
                var lat=(item['lat']);
                var lon=(item['lon']);
                mas_res2.push(lat);
                mas_res2.push(lon);
                $('#crew_id').val(item['crew_id']);
                
                var first_coord = $('#latlongmet').val();
                var regexp1 = new RegExp(/.*?,/);
                var matches1 = first_coord.match(regexp1);
                matches1 = String(matches1);
                matches1 = matches1.substring(0, matches1.length - 1);
                var mas_res = [];
                mas_res.push(matches1);
                
                var matches2 = first_coord.replace(regexp1, "");
                matches2 = String(matches2);
                mas_res.push(matches2);
                var markers2=[];
                markers2.push(mas_res);
                markers2.push(mas_res2);
                var coord_parts = markers[0].geometry.getCoordinates();
                var points2 = [];
                for(var i2 = 0, l2 = markers2.length; i2 < l2; i2++) {
                    points2[i2] = markers2[i2];
                }
                calc_near(points2, item['crew_id'], sort_mass, last_crew_wait);
                }
            })
        }
    });  	
}


function calc_near(points2, crew_id, sort_mass, counter){
        if(typeof route2 != "undefined"){
            route2.length = 0;
        }
        ymaps.route(points2, {
            mapStateAutoApply: true
        }).then(function (router2) {
            var coc_city = getCookie('gorod_landing');
            var gorod;
                gorod = 'almaty'; 
            $.ajax({
                url: 'get_info_crew.php?gorod='+gorod+'&crew_id='+crew_id,
                type: 'GET',
                dataType: 'JSON',
                success: function(data){
                    var class_name = data['data']['name'];
                    var regexp = new RegExp(/\[.*?\]/);
                    var matches = class_name.match(regexp);
                    matches = String(matches);
                    class_name = matches.substring(1, matches.length - 1);
                        
                    route2 = router2;
                        
                    var routeLength = route2.getLength(); // Длина маршрута
                    var routeLength = route2.getLength(); // Длина маршрута
                    var firstPath = route2.getPaths(); // Первый путь
                    var firstPathLength = route2.getLength(); // Длина первого пути
                    var firtstPathTime = route2.getTime(); // Время без учета пробок 
                    var firstPathJamsTime = route2.getJamsTime();// Время с пробками
                                    
                    var dlina_m = Math.ceil(routeLength);            
                    var gorod_name = $('#gorod_input').val();
                                    				          
                    dlina_m = (dlina_m % 100) >= 50 ? parseInt(dlina_m / 100) * 100 + 100 : parseInt(dlina_m / 100) * 100;
                    dlina_m=dlina_m/1000;
                    var obj = {'crew_id': crew_id, 'class_avto': class_name, 'dlina': dlina_m, 'vremya_s_pr': Math.ceil(firstPathJamsTime/60)}
                    sort_mass.push(obj);
                    function sortNumber(a, b){
                        return a - b;
                    }
                    var mass_dlin = [];
                    sort_mass.forEach(function(item, i, new_arr) {
                        mass_dlin.push(item['dlina']);
                        
                        
                        if(i == new_arr.length -1){
                            mass_dlin.sort(sortNumber);
                            var check_km1 = 0;
                            var check_km2 = 0;
                            for(var k = 0; k < mass_dlin.length; k++){
                                for(var j = 0; j < new_arr.length; j++){
                                    if(new_arr[j]['dlina'] == mass_dlin[k] && new_arr[j]['class_avto'] == "Ссанг-Йонг Kyron"){
                                        if(check_km1 == 0){
                                            var near_avto_mark = new_arr[j]['class_avto'];
                                            var time_podachi = new_arr[j]['vremya_s_pr'];
                                            var near_crew = new_arr[j]['crew_id'];
                                            $('#pod_cross').html("Подача: " + time_podachi + "мин.");
                                            check_km1 = 1;
                                            //console.log(near_crew + " -- " + near_avto_mark + " -- " + time_podachi);
                                        }
                                    }
                                    if(new_arr[j]['dlina'] == mass_dlin[k] && new_arr[j]['class_avto'] == "Рено_Логан"){
                                        if(check_km2 == 0){
                                            var near_avto_mark = new_arr[j]['class_avto'];
                                            var time_podachi = new_arr[j]['vremya_s_pr'];
                                            var near_crew = new_arr[j]['crew_id'];
                                            $('#pod_sedan').html("Подача: " + time_podachi + "мин.");
                                            check_km2 = 1;
                                            //console.log(near_crew + " -- " + near_avto_mark + " -- " + time_podachi); 
                                            //console.log("--------------------------------------");
                                        }
                                    }
                                }
                            }
                        }        
                                
                    })
                }
            })
    })  
}


function sendMail3(dataString)
{
    var url="sendMail.php?comment_client="+ dataString;
    $.ajax({  
        type: "GET",
        url: url,
        dataType: "html",
        cache: false,
        success: function(data) {
            var succ_res = '<div id="parent_popup4">\
                           <div id="myblock4" class="myClass4">\
                        <span style="float: right; margin-right: 10px; margin-top: 10px; cursor: pointer"><a id="open-close4"><img style="width: 40px;;" src="img/close.png" /></a></span>\
                            <div style="width: 300px; margin: auto;">\
                                <p style="margin-top: 30px; margin-left: 40px; margin-bottom: 30px; text-align: center; font-size: 28px; color: #2D9131; font-weight: 500; line-height: 0;">Спасибо!</p>\
                                <p style="text-align: center; font-size: 18px; line-height: 0;">Ваш запрос был принят.</p>\
                                <p style="text-align: center; font-size: 18px; line-height: 1; color: #848685;"></p>\
                            </div>\
                        </div>\
                        <div class="clear"></div>\
                    </div>';
            $('#for_email_success').html(succ_res);
            $('.myClass4').animate({'left':'40%'},550);
            $('#open-close4').click(function(event) {
            event.preventDefault();
                $('#myblock4').slideToggle();
                document.getElementById('parent_popup4').style.display='none';
                $('#email_client').val("");
                $('#name_client').val("");
                $('#comment_client').val("");
                $('.input_for_area3').css('border', '2px solid #2D9136');
                $('.region3').css('border', '2px solid #2D9136'); 
            });
        }
    });  
}

function checkmail(value) {
    reg = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    if (!value.match(reg)) {
        return "0"; 
    }
    else{
        return "1";
    }
}


function get_current_orders(){
        var url = "current_order.php";
        $.ajax({
            type: "GET",
            url: url,
            dataType: "html",
            cache: false,
            success: function(data) {
                $('#for_curr_table').html("");
                var jsonData = $.parseJSON(data);
                //console.log(jsonData['data']['orders']);
                if(jsonData['data']['orders'].length != 0){
                    var cur_table = '<table id="cur_table">\
                                    <thead>\
                                        <tr>\
                                            <th>Номер</th>\
                                            <th>Время создания</th>\
                                            <th>Время подачи</th>\
                                            <th>Адрес подачи</th>\
                                            <th>Адрес назначения</th>\
                                            <th>Пассажир</th>\
                                            <th>Комментарий</th>\
                                            <th>Состояние заказа</th>\
                                        </tr>\
                                    </thead>\
                                    <tbody id="for_cur_body">\
                                    </tbody>\
                                </table>';
                                
                    $('#for_curr_table').html(cur_table);
                    $('#cur_table').css('display', 'inherit');
                    jsonData['data']['orders'].forEach(function(item, i, arr) {
                        //console.log(item['start_time']);
                        var item_id = item['id'];
                        var item_start_time = item['start_time'];
                        var item_source_time = item['source_time'];
                        var item_source = item['source'];
                        var item_dest = item['destination'];
                        var item_passanger = item['passenger'];
                        var item_state = item['state_id'];
                        var item_comment = item['phone'];
                        var crew_id = item['crew_id'];
                        var time_length = item_start_time.length;

                        var item_source_time2 = moment([item_source_time.substring(0, time_length-10), item_source_time.substring(4, time_length-8)-1, item_source_time.substring(6, time_length-6), item_source_time.substring(8, time_length-4), item_source_time.substring(10, time_length-2)]);
                        item_source_time = item_source_time2.format('YYYY.MM.DD HH:mm');
                        
                        var item_start_time2 = moment([item_start_time.substring(0, time_length-10), item_start_time.substring(4, time_length-8)-1, item_start_time.substring(6, time_length-6), item_start_time.substring(8, time_length-4), item_start_time.substring(10, time_length-2)]);     
                        item_start_time = item_start_time2.format('YYYY.MM.DD HH:mm');
                        
                        var answer;
                        switch (item_state){
                            case 31:
                            case 30:
                            case 23:
                            case 1:
                                answer= "Заказ ожидает назначения автомобиля<br /><a style='cursor: pointer; float:left;cursor:pointer; width: 14px; margin-left: 10px; margin-top: 5px' id='delete_cur_order'><img style='width: 15px;' src='img/cancel_crew.png'/></a>";
                                break;
                            case 3:
                            case 2:
                            case 7:
                            case 8:
                                answer= "Водитель назначен на заказ<br /><a class='hide_link' onClick='view_map(" + crew_id + ");'><img src='img/car_ico.png' style='float:left;cursor:pointer; margin-top: 5px'/></a><a style='cursor: pointer; float:left;cursor:pointer; width: 14px; margin-left: 10px; margin-top: 5px' id='delete_cur_order'><img style='width: 15px;' src='img/cancel_crew.png'/></a>";
                                break;
                            case 10:
                            case 21:
                            case 18:
                            case 20:
                                answer= "Экипаж подъехал<br /><a class='hide_link' onClick='view_map(" + crew_id + ");'><img src='img/car_ico.png' style='float:left;cursor:pointer; margin-top: 5px'/></a><a style='cursor: pointer; float:left;cursor:pointer; width: 14px; margin-left: 10px; margin-top: 5px' id='delete_cur_order'><img style='width: 15px;' src='img/cancel_crew.png'/></a>";
                                break;
                            case 11:
                            case 16:
                            case 15:
                                answer= "Клиент в автомобиле<br /><a class='hide_link' onClick='view_map(" + crew_id + ");'><img src='img/car_ico.png' style='float:left;cursor:pointer; margin-top: 5px'/></a>";
                                break;
                            case 4:
                            case 17:
                                answer= "Выполнен";
                                break;
                            case 29:
                                answer= "Клиент не рассчитался";
                                break;
                            case 12:
                                answer= "Клиент не вышел";
                                break;
                            case 24:
                                answer= "Клиент отказался";
                                break;
                            case 6:
                                answer= "Отказано - нет машин";
                                break;
                            case 5:
                                answer= "Прекращен диспетчером";
                                break;
                        }
                        
                     var url2 = "get_comment.php?order_id="+item_id;
                     $.ajax({
                        type: "GET",
                        url: url2,
                        dataType: "html",
                        cache: false,
                        success: function(data2) {
                            var data_com = data2.replace(/\^/g,"<br />Тел: ");
                            var text_tr = "<tr>\
                                        <td>"+item_id+"</td>\
                                        <td>"+item_start_time+"</td>\
                                        <td>"+item_source_time+"</td>\
                                        <td>"+item_source+"</td>\
                                        <td>"+item_dest+"</td>\
                                        <td>"+item_passanger+"</td>\
                                        <td>"+data_com+"</td>\
                                        <td>"+answer+"</td>\
                                        </tr>";
                                $('#for_cur_body').prepend(text_tr);
                                $('#delete_cur_order').click(function(e){
                                    var succ_res = '<div id="parent_popup4">\
                                                   <div id="myblock4" class="myClass4">\
                                                <span style="float: right; margin-right: 10px; margin-top: 10px; cursor: pointer"></span>\
                                                    <div style="width: 300px; margin: auto;">\
                                                        <p style="margin-top: 30px; margin-bottom: 30px; text-align: center; font-size: 28px; color: #2D9131; font-weight: 500; line-height: 0;">Внимание!</p>\
                                                        <p style="text-align: center; font-size: 18px; line-height: 1;">Вы действительно хотите отменить заказ <br /> №'+ item_id +'.</p>\
                                                        <input type=button id="curr_yes" style="background: #2e9340; border-radius: 9px; color: #ffffff; cursor: pointer; font-size: 18px; font-weight: 500; padding: 8px 0 8px 4px; width: 90px; border: none; margin: 0 0 20px 50px;" value="Да"/>\
                                                        <input type=button id="curr_no" style="background: #2e9340; border-radius: 9px; color: #ffffff; cursor: pointer; font-size: 18px; font-weight: 500; padding: 8px 0 8px 4px; width: 90px; border: none; margin: 0 0 20px 20px;" value="Нет"/>\
                                                    </div>\
                                                </div>\
                                                <div class="clear"></div>\
                                            </div>';
                                    $('#for_delete_cur').html(succ_res);
                                    $('.myClass4').animate({'left':'40%'},550);
                                    $('#curr_no').click(function(event) {
                                        event.preventDefault();
                                        $('#myblock4').slideToggle();
                                        document.getElementById('parent_popup4').style.display='none';
                                    });
                                    
                                    $('#curr_yes').click(function(event) {
                                        var url3 = "delete_current_order.php?order_id="+item_id;
                                         $.ajax({
                                            type: "GET",
                                            url: url3,
                                            dataType: "html",
                                            cache: false,
                                            success: function(data3) { 
                                                //var j_data = $.parseJSON(data3);
//                                                console.log(data3);
                                                event.preventDefault();
                                                $('#myblock4').slideToggle();
                                                document.getElementById('parent_popup4').style.display='none';
                                                get_current_orders();
                                            }
                                         })
                                    });
                                })
                            }
                        })
                    });
                }
                else{
                    $('#cur_table').css('display', 'none');
                    $('#for_curr_table').prepend("<p style='text-align: center;'>Нет текущих заказов</p>");
                }       
            }
        })
}

function view_map(crew_id){
    var coc_city = getCookie('gorod_landing');
    var gorod;
        gorod = 'almaty'; 
    $.ajax({
        url: 'get_crew_coords.php?crew_id='+crew_id,
        type: 'GET',
        dataType: 'JSON',
        success: function(data){
            //console.log(data['data']['crews_coords'][0]);
            mas_current_crew=[];
            lat_current = data['data']['crews_coords'][0]['lat'];
            lon_current = data['data']['crews_coords'][0]['lon'];
            mas_current_crew.push(lat_current);
            mas_current_crew.push(lon_current);
            $('#for_curr_table').css('display', 'none');
            $('.for_map_current_order').css('display', 'inherit');
            if(view_map_param == 0){
                ymaps.ready(init2);
                view_map_param = 1;
            }  
        }
    })
}


function init2() {
    var myMap5 = new ymaps.Map('YMapsID2', {
        center: mas_current_crew, 
        zoom: 12,
        behaviors:['default', 'scrollZoom']
    }); 
        
    if(typeof(myPlacemark5) != "undefined"){
        myMap5.geoObjects.remove(myPlacemark);
    }

    var myPlacemark5 = new ymaps.Placemark(mas_current_crew, {hintContent: ''},
    {
        iconLayout: 'default#image',
        iconImageHref: '../img/car.png',
        iconImageSize: [35, 40],
        iconImageOffset: [0, 0]
    }); 
    myMap5.geoObjects.add(myPlacemark5);
                
    document.getElementById("hide_map5").style.display='block';
    ymaps.geocode(myPlacemark5.geometry.getCoordinates(), {
        results: 1
    }).then(function (res) {
        var firstGeoObject = res.geoObjects.get(0),
            coords3 = firstGeoObject.geometry.getCoordinates(),
            bounds = firstGeoObject.properties.get('boundedBy');
            myMap5.setBounds(bounds, {
            checkZoomRange: false
        });
    });
 }
 
function get_history_show(){
        var start_time = $('#time_start').val();
        var finish_time = $('#time_finish').val();
        
        var start_time = start_time.replace(/\./g, "");
        var start_time = start_time.replace(/\s/g, "");
        var start_time = start_time.replace(/\:/g, "");
        
        var finish_time = finish_time.replace(/\./g, "");
        var finish_time = finish_time.replace(/\s/g, "");
        var finish_time = finish_time.replace(/\:/g, "");
        
        var url = "history_data.php?start_time="+start_time+"&finish_time="+finish_time;
        $.ajax({
            type: "GET",
            url: url,
            dataType: "html",
            cache: false,
            success: function(data) {
                var jsonData = $.parseJSON(data);
                 $('#history_bottom').html("");
                if(jsonData['data']['orders'].length != 0){
                    var hist_table = '<table id="history_table">\
                                    <thead>\
                                        <tr>\
                                            <th>Номер</th>\
                                            <th>Время создания</th>\
                                            <th>Время подачи</th>\
                                            <th>Время завершения</th>\
                                            <th>Время пути</th>\
                                            <th>Адрес подачи</th>\
                                            <th>Адрес назначения</th>\
                                            <th>Пассажир</th>\
                                            <th>Состояние заказа</th>\
                                        </tr>\
                                    </thead>\
                                    <tbody id="for_history_body">\
                                    </tbody>\
                                </table>';
                    $('#history_bottom').html(hist_table);
                    $('#history_table').css('display', 'inherit');
                    jsonData['data']['orders'].forEach(function(item, i, arr) {
                        var item_id = item['id'];
                        var item_start_time = item['start_time'];
                        var item_source_time = item['source_time'];
                        var item_finish_time = item['finish_time'];
                        var item_source = item['source'];
                        var item_dest = item['destination'];
                        var item_passanger = item['passenger'];
                        var item_state = item['state_id'];
                        var time_length = item_start_time.length;
                        
                        var item_source_time3 = new Date(item_source_time.substring(0, time_length-10), item_source_time.substring(4, time_length-8)-1, item_source_time.substring(6, time_length-6), item_source_time.substring(8, time_length-4), item_source_time.substring(10, time_length-2), item_source_time.substring(12, time_length-0));
                        var item_source_time2 = moment([item_source_time.substring(0, time_length-10), item_source_time.substring(4, time_length-8)-1, item_source_time.substring(6, time_length-6), item_source_time.substring(8, time_length-4), item_source_time.substring(10, time_length-2)]);     
                        item_source_time = item_source_time2.format('YYYY.MM.DD HH:mm');
                        
                        var item_start_time3 = new Date(item_start_time.substring(0, time_length-10), item_start_time.substring(4, time_length-8)-1, item_start_time.substring(6, time_length-6), item_start_time.substring(8, time_length-4), item_start_time.substring(10, time_length-2), item_start_time.substring(12, time_length-0));
                        var item_start_time2 = moment([item_start_time.substring(0, time_length-10), item_start_time.substring(4, time_length-8)-1, item_start_time.substring(6, time_length-6), item_start_time.substring(8, time_length-4), item_start_time.substring(10, time_length-2)]);     
                        item_start_time = item_start_time2.format('YYYY.MM.DD HH:mm');
                        
                        var item_finish_time3 = new Date(item_finish_time.substring(0, time_length-10), item_finish_time.substring(4, time_length-8)-1, item_finish_time.substring(6, time_length-6), item_finish_time.substring(8, time_length-4), item_finish_time.substring(10, time_length-2), item_finish_time.substring(12, time_length-0));
                        var item_finish_time2 = moment([item_finish_time.substring(0, time_length-10), item_finish_time.substring(4, time_length-8)-1, item_finish_time.substring(6, time_length-6), item_finish_time.substring(8, time_length-4), item_finish_time.substring(10, time_length-2)]);     
                        item_finish_time = item_finish_time2.format('YYYY.MM.DD HH:mm');
                        
                        var milliseconds = 1000*60;
                        var millisBetween = item_finish_time3 - item_source_time3;
                        var minutes = Math.round(millisBetween / milliseconds);
                        var vremya_puti = minutes + " мин.";
                        
                        if(minutes <= 0){
                            vremya_puti = "00 мин.";
                        }
                        
                        if(minutes >= 60){
                            var houer = Math.floor(minutes/60);
                            minutes = minutes % 60;
                        }
                        if(houer >= 24){
                            var days = Math.floor(houer/24);
                            houer = houer % 24;
                        }
                        
                        if(typeof(houer) != "undefined"){
                             if(typeof(days) != "undefined"){
                                if(days == 1){
                                    
                                }
                                else{
                                    if(houer == 1){
                                        var vremya_puti = days + " дня " + houer + " час " + minutes + " мин.";
                                    }
                                    else{
                                        var vremya_puti = days + " дня " + houer + " часа " + minutes + " мин.";
                                    }
                                }
                             }
                             else{
                                if(houer == 1){
                                    var vremya_puti = houer + " час " + minutes + " мин.";
                                }
                                else{
                                    var vremya_puti = houer + " часа " + minutes + " мин.";
                                }
                             }
                        }
                        
                        var answer;
                        switch (item_state){
                            case 31:
                            case 30:
                            case 23:
                            case 1:
                                answer= "Заказ ожидает назначения автомобиля";
                                break;
                            case 3:
                            case 2:
                            case 7:
                            case 8:
                                answer= "Водитель назначен на заказ";
                                break;
                            case 10:
                            case 21:
                            case 18:
                            case 20:
                                answer= "Экипаж подъехал";
                                break;
                            case 11:
                            case 16:
                            case 15:
                                answer= "Клиент в автомобиле";
                                break;
                            case 4:
                            case 17:
                                answer= "Выполнен";
                                break;
                            case 29:
                                answer= "Клиент не рассчитался";
                                break;
                            case 12:
                                answer= "Клиент не вышел";
                                break;
                            case 24:
                                answer= "Клиент отказался";
                                break;
                            case 6:
                                answer= "Отказано - нет машин";
                                break;
                            case 5:
                                answer= "Прекращен диспетчером";
                                break;
                        }
                        
                        var text_tr = "<tr>\
                            <td>"+item_id+"</td>\
                            <td>"+item_start_time+"</td>\
                            <td>"+item_source_time+"</td>\
                            <td>"+item_finish_time+"</td>\
                            <td>"+vremya_puti+"</td>\
                            <td>"+item_source+"</td>\
                            <td>"+item_dest+"</td>\
                            <td>"+item_passanger+"</td>\
                            <td>"+answer+"</td>\
                            </tr>";
                    $('#for_history_body').prepend(text_tr);
                    });
                }
                else{
                    $('#history_table').css('display', 'none');
                    $('#history_bottom').prepend("<p style='text-align: center;'>По Вашему запросу ничего не найдено!</p>");
                }
            }
        })
}