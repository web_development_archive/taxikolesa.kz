

<div id="page_index">

	<div id="block1" class="block"><div>

		<div class="item1" style="top: 45px; left: 0px;">Служба такси в Алматы</div>

		<div class="item2" style="top: 162px; left: 690px;">Безопасная и комфортная поездка</div>
		<div class="item2" style="top: 214px; left: 690px;">Лучшие цены по городу</div>
		<div class="item2" style="top: 264px; left: 690px;">Всегда есть автомашины</div>

		<div class="item3" style="top: 337px; left: 676px;">Заказ такси по телефону:</div>

		<div class="item4" style="top: 373px; left: 676px;">
			+7 (727) 338-77-77<br />
			+7 (702) 221-30-30
		</div>

		<div class="item5" style="top: 352px; left: 470px;">
			Поездка
			<div>от 300 тг</div>
		</div>
	</div></div>


    <div id="content">
        <div id="glavn" style="width: 100%; background-color: #FFFFFF;">
            <div id="loader-wrapper">
                <div id="loader"></div>
            </div>
            <div id="form">
            </div>
            <div class="zakaz_taxi">
                <input type="hidden" id="address_name" value="">
                <form id="calltaxi-form" method="post">
                    <div style="display:none"><input type="hidden" value="1500d5bc7979e4504d7fd8f2f2394ffb910cfc73" name="YUPE_TOKEN"></div>
                    <div style="width: 96%; margin: auto;">
                        <div id="unsuccess" class="unsuccesss" style="background-color: #FFFFFF;">
                            <div id="un_left" style="float: left; width: 10%; margin-top: 10px; margin-left: 10px;"><img src="img/errors.png"></div>
                            <div id="un_right" style="float: right; width: 85%;"></div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div id="blog_zak_left">
                        <div id="karta_zakaza">
                            <div class="clear"></div>
                            <div class="territory">
                                <div class="contochka">
                                    <div class="km_puti"><p id="dlina_puti">0 км</p></div>
                                    <div class="minuta_ezdy"><p id="vremya_puti">0 мин</p></div>
                                </div>
                                <img src="img/4.png" class="contochka2">
                            </div>
                            <div class="nem_left">
                                <div class="input1">
                                    <label class="p_name">Откуда едем</label>
                                    <div class="znahi">
                                        <img id="samolet1" class="samolety" src="img/8.png">
                                        <ul id="nav">
                                            <li><a><img id="vokzal1" src="img/9.png"></a>
                                                <ul id="vokzaly_almaty">
                                                    <li id="almaty11"><a>Алматы 1</a></li>
                                                    <li id="almaty12"><a>Алматы 2</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="region" id="reg_from">
                                        <input placeholder="Адрес подачи" class="inp" name="SOURCE" id="CalltaxiForm_OrderSourceAddress" type="text" value="" autocomplete="off" style="background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3QsPDhss3LcOZQAAAU5JREFUOMvdkzFLA0EQhd/bO7iIYmklaCUopLAQA6KNaawt9BeIgnUwLHPJRchfEBR7CyGWgiDY2SlIQBT/gDaCoGDudiy8SLwkBiwz1c7y+GZ25i0wnFEqlSZFZKGdi8iiiOR7aU32QkR2c7ncPcljAARAkgckb8IwrGf1fg/oJ8lRAHkR2VDVmOQ8AKjqY1bMHgCGYXhFchnAg6omJGcBXEZRtNoXYK2dMsaMt1qtD9/3p40x5yS9tHICYF1Vn0mOxXH8Uq/Xb389wff9PQDbQRB0t/QNOiPZ1h4B2MoO0fxnYz8dOOcOVbWhqq8kJzzPa3RAXZIkawCenHMjJN/+GiIqlcoFgKKq3pEMAMwAuCa5VK1W3SAfbAIopum+cy5KzwXn3M5AI6XVYlVt1mq1U8/zTlS1CeC9j2+6o1wuz1lrVzpWXLDWTg3pz/0CQnd2Jos49xUAAAAASUVORK5CYII=); background-attachment: scroll; background-position: 100% 50%; background-repeat: no-repeat;">
                                        <div class="line_bg"><div class="inp_line"></div></div>
                                        <p class="for_inputs">Дом<a><img id="del1" class="immm" src="img/23.png"></a></p>
                                        <div class="clear"></div>
                                    </div>
                                    <a id="open_map" class="tochka1" title="Указать на карте Яндекс"><img src="img/6.png" class="ukz"></a>
                                    <div class="clear"></div>
                                </div>
                                <div class="a_mezhdu_nimi">
                                    <span class="obmen" style="display: none;"><a id="obmen_toch"><img src="img/22.png"></a></span>
                                    <span class="plus" style="display: none;"><a id="dop_addr"><img src="img/21.png"></a><p class="adddr">Добавить еще один адрес в маршрут</p></span>
                                    <div class="clear" style="display: none;"></div>
                                </div>
                                <div class="input2">
                                    <label class="p_name">Куда едем</label>
                                    <div class="znahi">
                                        <img id="samolet2" class="samolety" src="img/8.png">
                                        <ul id="nav">
                                            <li><a><img id="vokzal2" src="img/9.png"></a>
                                                <ul id="vokzaly_almaty">
                                                    <li id="almaty21"><a>Алматы 1</a></li>
                                                    <li id="almaty22"><a>Алматы 2</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="region" id="reg_to">
                                        <input placeholder="Адрес назначения" type="text" class="inp" name="DEST" id="CalltaxiForm_SourceAddress">
                                        <div class="line_bg"><div class="inp_line"></div></div>
                                        <p class="for_inputs">Дом<a><img id="del2" class="immm" src="img/23.png"></a></p>
                                        <div class="clear"></div>
                                    </div>
                                    <a id="open_map" class="tochka2" title="Указать на карте Яндекс"><img src="img/6.png" class="ukz"></a>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="line_zak"></div>
                        <img class="skidka22" src="img/10skidka.png" style="width: 143px;">
                        <div class="class_avto">
                            <h2 class="h2_t">Выберите класс машины</h2>

                            <div id="sedan"><div id="sedan1" class="class_mashiny" style="background: rgb(255, 255, 255);">
                                    <h3 style="color: rgb(45, 146, 63);">Toyota Corolla<a class="metka_info"><span class="classic" style="left: -4em; top: -9em;">Такси Колёса - беспрецедентно смелый и динамичный автомобиль Тойота Королла  2012 года<span style="top: 33px; left: -10px" class="triangle">▼</span></span><span class="sedan_icon"><img src="img/info.png"></span></a></h3>
                                    <div class="for_car"><img style="width: 170px; margin: 0 0 0 10px;" src="img/corolla.png"></div>
                                    <p class="podacha" id="pod_sedan" style="color: rgb(45, 146, 63);">1км - 50тг</p>
                                    <div class="clear"></div>
                                    <div class="ceny" style="background: rgb(244, 174, 10);">
                                        <p><span class="zach" id="sedan_zach">400</span><span class="skidka" id="sedan_skidka"> 300 т. </span></p>
                                    </div>
                                </div></div>
                            <div id="crossover"><div id="crossover1" class="class_mashiny" style="background: rgb(244, 174, 10);">
                                    <h3 style="color: rgb(255, 255, 255);">Toyota Camry<a class="metka_info"><span class="classic" style="left: -4em; top: -12em;">Такси Колёса – надежный и комфортабельный  бизнес класса  Тойота Камри  2012 года<span style="top: 33px; left: -50px" class="triangle">▼</span></span><span class="cross_icon"><img src="img/info2.png"></span></a></h3>
                                    <div class="for_car"><img style="width: 165px; margin: 0 0 0 10px;" src="img/2011-toyota-camry.png"></div>
                                    <p class="podacha" id="pod_cross" style="color: rgb(255, 255, 255);">1км - 75тг</p>
                                    <div class="ceny" style="background: rgb(244, 174, 10);">
                                        <p><span class="zach" id="cross_zach">500</span><span class="skidka" id="cross_skidka"> 300 т. </span></p>
                                    </div>
                                </div></div>
                            <div id="vip"><div id="vip1" class="class_mashiny" style=" background: rgb(244, 174, 10);">
                                    <h3 style="color: rgb(255, 255, 255);">Infinity Q 56<a class="metka_info"><span class="classic" style="left: -4em; top: -14em;">Такси Колёса VIP – автомобиль премиум  класса маркой  Infiniti, Lexus, Mercedez-Benz, BMW<span style="top: 33px; left: 52px" class="triangle">▼</span></span><span class="vip_icon"><img src="img/info2.png"></span></a></h3>
                                    <div class="for_car"><img style="width: 190px; margin: 30px 10px 0 0;" src="img/inf.png"></div>
                                    <p class="podacha" style="color: rgb(255, 255, 255);">Предзаказ</p>
                                    <div class="ceny" style="background: rgb(244, 174, 10);">
                                        <p><span class="zach" id="vip_zach"></span><span class="skidka" id="vip_skidka">14000 тг/час</span></p>
                                    </div>
                                </div></div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div id="for_map">
                        <div id="yandex_map">
                            <div id="YMapsID" style="width:505px; height: 550px;  padding: 20px 0;  overflow: hidden; border-radius: 45px !important;"></div>
                            <div id="coord_form">
                                <input id="latlongmet" class="input-medium" name="icon_text1" type="hidden">
                                <input id="latlongmet2" class="input-medium" name="icon_text2" type="hidden">
                                <input id="crew_id" class="input-medium" name="icon_text3" type="hidden">
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="line_zak"></div>
                    <h2 class="dp_tr"><span class="sver"><img src="img/24.png"></span>Дополнительные требования</h2>
                    <div class="clear"></div>
                    <div class="vremya_zakaza">
                        <div class="left_time">
                            <div class="nahu">
                                <div class="time_form">
                                    <div class="changer_time" style="left: 0px; width: 80px;"></div>
                                    <div class="for_time1"></div>
                                    <div class="for_time2"></div>
                                    <div class="time_form2" onclick="change_time(1)" id="time1" style="color: rgb(0, 0, 0);">Сейчас</div>
                                    <div class="time_form3" onclick="change_time(2)" id="time2" style="color: rgb(202, 216, 204);">Дата и время</div>
                                    <div class="clear"></div>
                                </div>
                                <div class="sel_time" style="display: inherit;">
                                    <h3>Когда вам подать машину?</h3>
                                    <a><div onclick="select_time(1)" id="minuty1" class="minuty" style="color: rgb(0, 0, 0); background: rgb(252, 166, 23);"><p style="margin-top: 23px;">Сейчас</p></div></a>
                                    <a><div onclick="select_time(2)" id="minuty2" class="minuty" style="color: rgb(255, 255, 255); background: rgb(24, 87, 40);"><p><span class="mmm">+15</span><br>минут</p></div></a>
                                    <a><div onclick="select_time(3)" id="minuty3" class="minuty" style="color: rgb(255, 255, 255); background: rgb(24, 87, 40);"><p><span class="mmm">+30</span><br>минут</p></div></a>
                                    <a><div onclick="select_time(4)" id="minuty4" class="minuty" style="color: rgb(255, 255, 255); background: rgb(24, 87, 40);"><p><span class="mmm">+45</span><br>минут</p></div></a>
                                    <div class="clear"></div>
                                </div>

                                <div class="sel_time2" style="display: none;">
                                    <h3>Когда вам подать машину?</h3>
                                    <input placeholder="Время подачи" class="time_sel" value="" onclick="NewCssCal('CalltaxiForm_OrderTime', 'yyyyMMdd', 'arrow', true, '24', false);" name="ORDER_TIME" id="CalltaxiForm_OrderTime" type="text">
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>

                        <div class="right_time">
                            <div class="nahu">
                                <p class="spsb">Дополнительные требования:</p>
                                <select class="inp2" name="sposoby_opl" id="sposoby_opl">
                                    <option value="">Выберите требование</option>
                                    <option value="Встреча с табличкой (+ 200 тг)">Встреча с табличкой (+ 200 тг)</option>
                                    <option value="Услуга трезвый водитель (+ 1 200 тг)">Услуга трезвый водитель (+ 1 200 тг)</option>
                                    <option value="Багаж в салоне (+ 200 тг)">Багаж в салоне (+ 200 тг)</option>
                                    <option value="Поездка с животными (бесплатно)">Поездка с животными (бесплатно)</option>
                                    <option value="Брендированный автомобиль (бесплатно)">Брендированный автомобиль (бесплатно)</option>
                                    <option value="Молчаливый водитель (бесплатно)">Молчаливый водитель (бесплатно)</option>
                                    <option value="Деликатное вождение (бесплатно)">Деликатное вождение (бесплатно)</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="dop_trebovaniya">
                        <div class="dp">
                            <p>Имя</p>
                            <div id="region_name" style="border-radius: 6px;">
                                <input class="inp3" placeholder="Введите ваше имя" name="PASSENGER" id="PASSENGER" type="text" maxlength="75">
                                <div class="clear"></div>
                            </div>
                            <p class="spsb2">Телефон</p>
                            <div id="region_tel" style="height: 36px; border-radius: 6px;">
                                <input type="text" placeholder="Введите ваш номер телефона" class="inp2" maxlength="18" name="PHONE_TO_DIAL" id="PHONE_TO_DIAL" value="">        <!--PHONE_TO_DIAL-->
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="dp">
                            <p>Комментарий к заказу</p>
                            <div id="region_com" style="border-radius: 6px;">
                                <textarea placeholder="Тут можно указать где      Вас ожидать..." class="txt4" name="COMMENT" id="COMMENT" maxlength="400"></textarea>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div class="clear"></div>
                    <div class="line_zak"></div>
                    <div class="zakazata">
                        <div class="end">
                            <img title="Очистка формы" alt="Очистка формы" src="img/5.png" id="form_clear">
                            <label for="form_clear" id="lab">Удалить заказ</label>
                            <input type="button" class="zak" id="submit_client" value="ЗАКАЗАТЬ" name="send">
                        </div>
                        <div class="clear"></div>
                    </div>
                </form>
            </div>
        </div>


        <div id="onas" style="width: 100%; display: none;">
            <img src="img/img2/komandir.png" width="100%">
            <div class="name_blg">
                <div class="bgg1"><div class="line_on"></div></div>
                <div class="bgg2"><h2>О компании</h2></div>
                <div class="bgg3"><div class="line_on"></div></div>
            </div>
            <div class="clear"></div>
            <div class="text_onas">
                <p>Наша компания существует с 2009 года, на начальном этапе в таксопарке компании имелось 20 автомобилей марки “ToyotaCamry 40”</p>
                <p>Позже руководством компании было принято решение закупить партию отечественных автомобилей марки “SsangYongKyron”, данные авто пришлись по вкусу всем и с того времени компания начала бурно развиваться при поддержке Акимата города Алматы. На данный момент компания является полностью компьютеризированной.</p>
                <p>В начале августа 2013 года в Алматы был открыт таксопарк ”Эко Такси”, а уже 1 декабря наш таксопарк был презентован и открыт в Астане.</p>
                <p>Экологические такси SsangYong были протестированы лично главой государства, которого сопровождал аким Астаны Имангали Тасмагамбетов. Автомобили, задействованные в проекте - SsangYongKyron, оснащены газовым оборудованием из Италии, специальным таксометром и навигаторами. Заказывать такое такси можно онлайн, воспользовавшись, кстати, приложениями под iOS и Android. Из современных технологий задействованы также и возможности уведомлениями пассажиров о встречающей их машине посредством смс-сообщений и смс-чеков.</p>
                <p>Таким образом, наша компания выходит на уровень муниципального такси, а наш проект "ЭКО ТАКСИ" является одной из трех программ акимата, по подготовке инфраструктуры города Астана к проведению международной выставки EXPO-2017. Сейчас на линии в Астане работает 100 машин, а в Алматы 200 машин. К 2017 году при поддержке государства их количество будет доведено до 500.</p>
            </div>
            <div class="line_on"></div>
            <div class="youtube">
                <iframe width="1200px" height="300px" src="//www.youtube.com/embed/XQVWSsiAOIs" frameborder="0" allowfullscreen=""></iframe>
            </div>
        </div>


        <div id="tarif" style="width: 100%; min-height: 600px; display: none;">
            <div class="name_blg">
                <div class="bgg1"><div class="line_on"></div></div>
                <div class="bgg2"><h2>Тарифы</h2></div>
                <div class="bgg3"><div class="line_on"></div></div>
            </div>
            <div class="clear"></div>
            <div class="tarify" style="display: inherit;">
                <table>
                    <thead>
                    <tr><th class="tarif_osnovnoi" colspan="2">Тариф "Седан"</th>
                    </tr></thead>
                    <tbody>
                    <tr>
                        <td class="tarif_name">Проезд до 5 км</td>
                        <td class="tarif_cena">590 тг</td>
                    </tr>
                    <tr class="polosa_color">
                        <td class="tarif_name">1 км</td>
                        <td class="tarif_cena">90 тг</td>
                    </tr>
                    <tr>
                        <td class="tarif_name">Ожидание 10 мин.</td>
                        <td class="tarif_cena">Бесплатно</td>
                    </tr>
                    <tr class="polosa_color">
                        <td class="tarif_name">Дальнейшее ожидание</td>
                        <td class="tarif_cena">25 тг/мин</td>
                    </tr>
                    <tr>
                        <td class="tarif_name">Трансфер "Аэропорт" <br>(22 км. включительно)</td>
                        <td class="tarif_cena">2500 тг</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="tarify" style="display: inherit;">
                <table>
                    <thead>
                    <tr><th class="tarif_osnovnoi" colspan="2">Тариф "Кроссовер"</th>
                    </tr></thead>
                    <tbody>
                    <tr>
                        <td class="tarif_name">Проезд до 5 км</td>
                        <td class="tarif_cena">750 тг</td>
                    </tr>
                    <tr class="polosa_color">
                        <td class="tarif_name">1 км</td>
                        <td class="tarif_cena">105 тг</td>
                    </tr>
                    <tr>
                        <td class="tarif_name">Ожидание 10 мин.</td>
                        <td class="tarif_cena">Бесплатно</td>
                    </tr>
                    <tr class="polosa_color">
                        <td class="tarif_name">Дальнейшее ожидание</td>
                        <td class="tarif_cena">25 тг/мин</td>
                    </tr>
                    <tr>
                        <td class="tarif_name">Трансфер "Аэропорт" <br>(22 км. включительно)</td>
                        <td class="tarif_cena">3000 тг</td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div class="tarify" style="display: inherit;">
                <table>
                    <thead>
                    <tr><th class="tarif_osnovnoi" colspan="2">Тариф "Алматы - область"</th>
                    </tr></thead>
                    <tbody>
                    <tr>
                        <td class="tarif_name">Каскелен</td>
                        <td class="tarif_cena">1,250 тг</td>
                    </tr>
                    <tr class="polosa_color">
                        <td class="tarif_name">Бурундай</td>
                        <td class="tarif_cena">1,000 тг</td>
                    </tr>
                    <tr>
                        <td class="tarif_name">ГРЭС пос. «Отеген Батыр»</td>
                        <td class="tarif_cena">1,250 тг.</td>
                    </tr>
                    <tr class="polosa_color">
                        <td class="tarif_name">Пив. завод Эфес</td>
                        <td class="tarif_cena">1,000 тг</td>
                    </tr>
                    <tr>
                        <td class="tarif_name">Талгар</td>
                        <td class="tarif_cena">1,000 тг</td>
                    </tr>
                    <tr class="polosa_color">
                        <td class="tarif_name">Акбулак (за Талгаром)</td>
                        <td class="tarif_cena">2,000 тг</td>
                    </tr>
                    <tr>
                        <td class="tarif_name">Иссык</td>
                        <td class="tarif_cena">2,000 тг</td>
                    </tr>
                    <tr class="polosa_color">
                        <td class="tarif_name">Фабричный</td>
                        <td class="tarif_cena">2,000 тг</td>
                    </tr>
                    <tr>
                        <td class="tarif_name">г. Капчагай</td>
                        <td class="tarif_cena">4,200 тг</td>
                    </tr>
                    <tr class="polosa_color">
                        <td class="tarif_name">г. Капчагай - зоны отдыха</td>
                        <td class="tarif_cena">5,500 тг</td>
                    </tr>
                    <tr>
                        <td class="tarif_name">Форелевое хозяйство</td>
                        <td class="tarif_cena">5,000 тг</td>
                    </tr>
                    <tr class="polosa_color">
                        <td class="tarif_name">Чилик</td>
                        <td class="tarif_cena">7,500 тг</td>
                    </tr>
                    <tr>
                        <td class="tarif_name">Байсерке (Дмитриевка)</td>
                        <td class="tarif_cena">1,250 тг</td>
                    </tr>
                    <tr class="polosa_color">
                        <td class="tarif_name">Институт ядерной физики, ПИТ «Ала-Тау»</td>
                        <td class="tarif_cena">750 тг</td>
                    </tr>
                    <tr>
                        <td class="tarif_name">Красное поле</td>
                        <td class="tarif_cena">700 тг</td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div class="tarify" style="display: inherit;">
                <table>
                    <thead>
                    <tr><th class="tarif_osnovnoi" colspan="2">Тариф "Вызов за пределы Алматы"</th>
                    </tr></thead>
                    <tbody>
                    <tr>
                        <td class="tarif_name"><b>Жилые комплексы:</b> «Люксор», «Тау-Самал», «Аспан-Тау», «Кристалл-Эйр», «Мирас», «Аль-Фараби», «Солнечная долина», «Арман», Ж/К Европолис, Ж/К Сан – Таун, Микр.Кок-Тобе, отель «Royal Tulip»</td>
                        <td class="tarif_cena">250 тг</td>
                    </tr>
                    <tr class="polosa_color">
                        <td class="tarif_name">«Асем-Тау»</td>
                        <td class="tarif_cena">400 тг</td>
                    </tr>
                    <tr>
                        <td class="tarif_name"><b>Рестораны:</b> «Биладжио», «Алаша», «Сабантуй», «Самарканд»</td>
                        <td class="tarif_cena">250 тг.</td>
                    </tr>
                    <tr class="polosa_color">
                        <td class="tarif_name"><b>Микрорайоны:</b> «Калкаман», «Кооп. техникум», «Ерменсай», «Каргалинка», ,Айнабулак</td>
                        <td class="tarif_cena">550 тг</td>
                    </tr>
                    <tr>
                        <td class="tarif_name">«Верхняя Каменка»</td>
                        <td class="tarif_cena">400 тг</td>
                    </tr>
                    <tr class="polosa_color">
                        <td class="tarif_name"><b>Санатории:</b> «Алатау», «Алмалы», «Алматы», «Коктем</td>
                        <td class="tarif_cena">750 тг</td>
                    </tr>
                    <tr>
                        <td class="tarif_name">Каменское плато (санаторий Турксиб)</td>
                        <td class="tarif_cena">1,000 тг</td>
                    </tr>
                    <tr class="polosa_color">
                        <td class="tarif_name">Алматы-1, «Акжар», мкр-н «Айнабулак», «Ак-Каин»</td>
                        <td class="tarif_cena">400 тг</td>
                    </tr>
                    <tr>
                        <td class="tarif_name">Нижняя пятилетка, ВАЗ, «Камаз-Центр», «Первомайка», «Алтай»</td>
                        <td class="tarif_cena">550 тг</td>
                    </tr>
                    <tr class="polosa_color">
                        <td class="tarif_name">«Жулдыз» </td>
                        <td class="tarif_cena">700 тг</td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div class="tarify_ast" style="display: none;">
                <table>
                    <thead>
                    <tr><th class="tarif_vtr" colspan="2">Тариф "Основной"</th>
                    </tr></thead>
                    <tbody>
                    <tr>
                        <td class="tarif_name">Проезд до 5 км</td>
                        <td class="tarif_cena">750 тг</td>
                    </tr>
                    <tr class="polosa_color">
                        <td class="tarif_name">Бордюр</td>
                        <td class="tarif_cena">200тг/3км</td>
                    </tr>
                    <tr>
                        <td class="tarif_name">По городу Астана свыше 5 км</td>
                        <td class="tarif_cena">105 тг/км</td>
                    </tr>
                    <tr class="polosa_color">
                        <td class="tarif_name">За пределами Астана свыше 5 км</td>
                        <td class="tarif_cena">150 тг/км</td>
                    </tr>
                    <tr>
                        <td class="tarif_name">Ожидание 10 мин.</td>
                        <td class="tarif_cena">Бесплатно</td>
                    </tr>
                    <tr class="polosa_color">
                        <td class="tarif_name">Дальнейшее ожидание</td>
                        <td class="tarif_cena">25 тг/мин</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div id="korp" style="width: 100%; display: none;">
            <div id="for_email_success"></div>
            <div class="clear"></div>
            <div class="name_blg2">
                <div class="bgg111"><div class="line_on"></div></div>
                <div class="bgg222"><h2>Наш сервис</h2></div>
                <div class="bgg333"><div class="line_on"></div></div>
            </div>
            <div class="clear"></div>

            <div class="korp_blog">
                <div class="kpb">
                    <div class="k_img"><img src="img/img2/1.png"></div>
                    <div class="k_text">Заявки корпоративных клиентов обслуживается в приоритетном порядке.</div>
                </div>
                <div class="kpb">
                    <div class="k_img"><img src="img/img2/4.png"></div>
                    <div class="k_text">Компьютеризированный учет заказов позволяет предоставить максимально полную и подробную отчетность.</div>
                </div>
                <div class="kpb">
                    <div class="k_img"><img src="img/img2/7.png"></div>
                    <div class="k_text">Предоставление профессионального диспетчера для организации квалифицированного обслуживания Вашей компании.</div>
                </div>
            </div>
            <div class="clear"></div>
            <div class="korp_blog">
                <div class="kpb">
                    <div class="k_img"><img src="img/img2/2.png"></div>
                    <div class="k_text">Современный Call-Центр автоматически ставит Ваш звонок первым на очереди и переводит его на старшего диспетчера, занимающегося обслуживанием корпоративных клиентов.</div>
                </div>
                <div class="kpb">
                    <div class="k_img"><img src="img/img2/5.png"></div>
                    <div class="k_text">Расчет стоимости поездки ведется таксометром на основании пройденных километров и времени ожидания, что делает не выгодно водителю "медленно плестись", как в случае с повременной оплатой и делает вашу поездку максимально быстрой.</div>
                </div>
                <div class="kpb">
                    <div class="k_img"><img src="img/img2/8.png"></div>
                    <div class="k_text">Все машины в нашем парке оснащены таксометрами, на экране которых отображается полная информация о Вашей поездке с указанием километража в городе и области, затраченного времени в пути и суммы заказа.</div>
                </div>
            </div>
            <div class="clear"></div>
            <div class="korp_blog">
                <div class="kpb">
                    <div class="k_img"><img src="img/img2/3.png"></div>
                    <div class="k_text">Автомобили компании оборудованы спутниковой системой слежения, которая позволяет диспетчеру грамотно и оперативно направить ближайшую машину по указанному адресу клиента.</div>
                </div>
                <div class="kpb">
                    <div class="k_img"><img src="img/img2/6.png"></div>
                    <div class="k_text">В наших машинах нет шумных радиостанций, все заказы поступают водителю на мобильный терминал по GPRS каналу.</div>
                </div>
                <div class="kpb">
                    <div class="k_img"><div style="background-color: #2D9343; border-radius: 30px; width: 65px;"><img src="img/img2/555.png"></div></div>
                    <div class="k_text">GPS-датчики позволяют отслеживать место расположения и все передивжения машин</div>
                </div>
            </div>
            <div class="clear"></div>
            <div class="name_blg3">
                <div class="bgg1111"><div class="line_on"></div></div>
                <div class="bgg2222"><h2>Наши преимущества</h2></div>
                <div class="bgg3333"><div class="line_on"></div></div>
            </div>
            <div class="clear"></div>
            <div class="korp_blog">
                <div class="kpb">
                    <div class="k_img"><img src="img/img2/9.png"></div>
                    <div class="k_text">Парк автомобилей бизнес класса удовлетворят любые потребности Вашей компании.</div>
                </div>
                <div class="kpb">
                    <div class="k_img"><img src="img/img2/12.png"></div>
                    <div class="k_text">Современный Call-Центр, позволяющий профессионально обрабатывать телефонные звонки.</div>
                </div>
                <div class="kpb">
                    <div class="k_img"><img src="img/img2/15.png"></div>
                    <div class="k_text">Компьютеризованная диспетчерская на основе новейших программ для служб такси.</div>
                </div>
            </div>
            <div class="clear"></div>
            <div class="korp_blog">
                <div class="kpb">
                    <div class="k_img"><img src="img/img2/10.png"></div>
                    <div class="k_text" style="margin-top: 20px;">Удобная расшифровка поездок.</div>
                </div>
                <div class="kpb">
                    <div class="k_img"><img src="img/img2/13.png"></div>
                    <div class="k_text" style="margin-top: 20px;">Гибкая система скидок.</div>
                </div>
                <div class="kpb">
                    <div class="k_img"><img src="img/img2/16.png"></div>
                    <div class="k_text">Наличие мобильных терминалов, позволяющих водителям принимать заказы не находясь в машине.</div>
                </div>
            </div>
            <div class="clear"></div>
            <div class="korp_blog">
                <div class="kpb">
                    <div class="k_img"><img src="img/img2/11.png"></div>
                    <div class="k_text">Все автомобили в нашем парке всегда находятся в отличном техническом и идеальном внешнем состоянии.</div>
                </div>
                <div class="kpb">
                    <div class="k_img"><img src="img/img2/14.png"></div>
                    <div class="k_text">Всегда к вашим услугам опытные, профессиональные водители с большим опытом работы.</div>
                </div>
            </div>
            <div class="clear"></div>
            <div class="name_blg4">
                <div class="bgg11111"><div class="line_on"></div></div>
                <div class="bgg22222"><h2>По вопросам сотрудничества обращайтесь</h2></div>
                <div class="bgg33333"><div class="line_on"></div></div>
            </div>
            <div class="clear"></div>
            <div style="margin-left: 30%;">
                <div class="clear"></div>
                <div class="xc"><img class="xxxxx" src="img/img2/18.png"> 2 900 500 доб.1</div>
                <div class="xc"><img class="xxxxx" src="img/img2/17.png"> info@komandir.kz</div>
                <div class="clear"></div>
            </div>
            <p style="font-weight: 800; text-align: center; line-height: 3;">или закажите звонок</p>
            <div style="width: 800px; margin-left: 20%; margin-bottom: 90px;">
                <input placeholder="Имя" type="text" class="name_z" style="height: 40px; font-size: 18px; padding: 5px; border: 2px solid #2E9343;">
                <input placeholder="Телефон" type="text" class="tell_z" style="margin-left: 30px; height: 40px; font-size: 18px; padding: 5px; border: 2px solid #2E9343;">
                <input type="button" class="zakaz_z" value="ЗАКАЗАТЬ" style="cursor: pointer; margin-left: 30px; width: 180px; height: 40px; font-size: 18px; padding: 5px; border: 2px solid #2E9343; background: #2E9343; color: #FFFFFF">
            </div>
        </div>
    </div>


    <div id="block2" class="block"><div>

		<div class="item5" style="top: 39px; left: 471px;"><img src="<?php echo base_url()?>media/design/index_block2_item1.png"></div>
		<div class="item1" style="top: 55px; left: 0px;">Наши услуги</div>

		<div class="item2" style="top: 204px; left: 170px;"><img src="<?php echo base_url()?>media/design/index_block2_item2_1.png"></div>
		<div class="item2" style="top: 167px; left: 385px;"><img src="<?php echo base_url()?>media/design/index_block2_item2_2.png"></div>
		<div class="item2" style="top: 161px; left: 616px;"><img src="<?php echo base_url()?>media/design/index_block2_item2_3.png"></div>
		<div class="item2" style="top: 168px; left: 840px;"><img src="<?php echo base_url()?>media/design/index_block2_item2_4.png"></div>

		<div class="item3" style="top: 300px; left: 170px;">
			Такси<br />
			по городу
		</div>
		<div class="item3" style="top: 300px; left: 385px;">
			Услуга<br />
			трезвого<br />
			водителя
		</div>
		<div class="item3" style="top: 300px; left: 616px;">
			Почасовое<br />
			такси
		</div>
		<div class="item3" style="top: 300px; left: 840px;">
			Трансфер<br />
			Центр города -<br />
			Аэропорт
		</div>

		<div class="item4" style="top: 372px; left: 170px;"><a href="<?php echo base_url()?>tariffs#city">от 300 тг</a></div>
		<div class="item4" style="top: 372px; left: 385px;"><a href="<?php echo base_url()?>trezv">от 1200 тг</a></div>
		<div class="item4" style="top: 372px; left: 616px;"><a href="<?php echo base_url()?>tariffs#hour">от 2200 тг</a></div>
		<div class="item4" style="top: 372px; left: 840px;"><a href="<?php echo base_url()?>tariffs#airport">от 1100 тг</a></div>
	</div></div>

	<div id="block3" class="block"><div>

		<div class="item1" style="top: 153px; left: 0px;"><img src="<?php echo base_url()?>media/design/index_block3_item4.png"></div>
		<div class="item2" style="top: 98px; left: 0px;">Корпоративная программа</div>
		<div class="item3" style="top: 170px; left: 0px;">Бизнес Такси Колёса</div>
		<div class="item4" style="top: 287px; left: 0px;"><a href="<?php base_url()?>business">Подробнее</a></div>
	</div></div>

	<div class="block" id="block13"><div>

		<div class="item1" style="top: 39px; left: 0px;">Пресса о нас</div>
		<div class="item2" style="top: 135px; left: 150px;"><a href="http://www.kaznu.kz/ru/3/news/one/8163/" target="_blank"><img src="media/design/block13_item4.png" alt=""></a></div>
		<div class="item3" style="top: 314px; left: 150px;">
			<a href="http://www.kaznu.kz/ru/3/news/one/8163/" target="_blank"><b>Женщины-ветераны ВОВ прошли<br />
				бесплатное обследование в КазНУ</b></a><br /><br />
			Двенадцать жительниц Бостандыкского района -<br />
            ветеранов Великой Отечественной войны и <br />
            участниц трудового фронта - смогли бесплатно<br />
            пройти обследование в медицинском <br />
            диагностическом центре «Smart Health»<br />
            при КазНУ им. аль-Фараби. В университет их<br />
            доставила служба такси «Колеса».
		</div>

        <div class="item2" style="top: 135px; left: 470px;"><a href="http://www.kaznu.kz/ru/14951/news/one/8134/" target="_blank"><img src="media/design/block13_item6.png" alt=""></a></div>
		<div class="item3" style="top: 314px; left: 470px;">
			<a href="http://www.kaznu.kz/ru/14951/news/one/8134/" target="_blank"><b>В КазНУ состоится благотворительная<br />
				акция «Твори добро!»<br />
				для женщин-ветеранов</b></a><br />
			Мероприятие проводится в канун<br />
			Международного женского дня 8 марта<br />
			работниками медицинского диагностического<br />
			центра «Smart Health», открытого<br />
			в университете совместно<br />
			с южнокорейской клиникой «Кагнам Северанс»<br />
			(Ю.Корея).
		</div>

		<div class="item2" style="top: 135px; left: 790px;"><a href="http://inform.kz/rus/article/2751823" target="_blank"><img src="media/design/block13_item5.png" alt=""></a></div>
		<div class="item3" style="top: 314px; left: 790px;">
			<a href="http://inform.kz/rus/article/2751823" target="_blank"><b>В Алматы студенты устроили<br />
				оздоровительный флешмоб</b></a><br />
			В Алматы в Казахском национальном<br />
			университете им. аль-Фараби прошел<br />
			молодежный флешмоб. Мероприятие<br />
			организовал медицинский диагностический<br />
			центр «Smart Health». В акции приняли<br />
			участие студенты и художественные<br />
			коллективы вуза. Они подготовили<br />
			концертную программу с песнями<br />
			и танцами. 
		</div>		

	</div></div>
</div>