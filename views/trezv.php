<div id="page_trezv">

	<div id="block1" class="block"><div>

		<div class="item1" style="top: 49px; left: 365px;">Услуга трезвого водителя</div>

		<div class="item2" style="top: 122px; left: 365px;">
			Если твой вечер завершился более празднично,<br />
			чем ты рассчитывал... или тебе нужен ответственный<br />
			подрядчик для доставки авто?<br />
			Нет повода для беспокойства!<br />
			Мы доставим автомобиль и водителя<br />
			по нужному адресу<br />
			<br />
			БЫСТРО, ОТВЕТСТВЕННО, НЕДОРОГО!
		</div>

		<div class="item3" style="top: 356px; left: 365px;">
			<b>Нам можно доверять!</b><b><br />
			<br />
			Конкурсный отбор</b><br />
			Мы отбираем водителей и операторов<br />
			на основании конкурса,<br />
			из сотен резюме к нам устраиваются<br />
			единицы лучших!<b><br />
			<br />
			Стаж от 10 лет</b><br />
			Мы принимаем в штат водителей<br />
			со стажем 10 и более лет,<br />
			с максимальным профессионализмом<br />
			и опытом за рулём!<b><br />
			<br />
			Минимальное ожидание</b><br />
			Наш водитель приедет к вам<br />
			за считанные минуты, или минимально<br />
			возможное время, где бы вы<br />
			ни находились!<b><br />
			<br />
			Call-Center 24/7</b><br />
			Наши операторы работают<br />
			круглосуточно! Мы имеем достаточное<br />
			количество линий, чтобы оперативно<br />
			реагировать на звонки!<b><br />
			<br />
			Все марки авто</b><br />
			Наши водители управляют<br />
			любой маркой автомобиля.<br />
			любой категории.<br />
			С любым расположением руля.
		</div>

		<div class="item4" style="top: 1016px; left: 365px;">
			Закажи трезвого водителя ПРЯМО СЕЙЧАС!
		</div>

		<div class="item5" style="top: 1071px; left: 380px;"><img src="<?php echo base_url()?>media/design/trezv_block1_item5_1.png"></div>
		<div class="item5" style="top: 1071px; left: 553px;"><img src="<?php echo base_url()?>media/design/trezv_block1_item5_2.png"></div>
		<div class="item5" style="top: 1071px; left: 729px;"><img src="<?php echo base_url()?>media/design/trezv_block1_item5_3.png"></div>

		<div class="item6" style="top: 1141px; left: 380px;">
			Позвони<br />
			по указанному<br />
			номеру
		</div>
		<div class="item6" style="top: 1141px; left: 553px;">
			Сообщи свое<br />
			местоположение<br />
			и телефон
		</div>
		<div class="item6" style="top: 1141px; left: 729px;">
			Расслабься<br />
			и жди<br />
			водителя
		</div>

		<div class="item7" style="top: 1242px; left: 365px;">
			<b>не отказывай себе в празднике!</b><br />
			доверь автомобиль, права<br />
			и свою жизнь профессионалам!
		</div>
		<div class="item8" style="top: 1363px; left: 533px;">
			+7 702 221 30 30<br />
			+7 702 221 30 33
		</div>
		<div class="item9" style="top: 1336px; left: 399px;"><img src="<?php echo base_url()?>media/design/trezv_block1_item9.png"></div>

	</div></div>
</div>