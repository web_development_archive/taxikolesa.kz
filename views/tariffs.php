<div id="page_tariffs">

	<div id="block1" class="block"><div>

		<div class="item1" style="top: 49px; left: 365px;">Услуги и тарифы</div>

		<div class="item2" id="city" style="top: 131px; left: 365px;">
			Такси по городу от 300 тг
		</div>
		<div class="item2" id="airport" style="top: 299px; left: 365px;">
			Такси в аэропорт от 1100 тг
			<div>(300 тг парковка оплачивается клиентом)</div>
		</div>
		<div class="item2" style="top: 380px; left: 365px;">
			Такси по пригороду Алматы<br />
			и области от 1500 тг
		</div>
		<div class="item2" id="hour" style="top: 681px; left: 365px;">
			Почасовое такси от 2200 тг<br />
			<div>(минимальный заказ 2 часа,<br />
			каждый последующий час по 1100 тг)</div>
		</div>
		<div class="item2" style="top: 828px; left: 365px;">
			Услуга трезвого водителя<br />
			от 1500 тг
		</div>

		<div class="item3" style="top: 170px; left: 387px;">
			Центр - Медео 1500 тг<br />
			Центр - Чимбулак 3000 тг<br />
			Центр - Алмарасан 1500 тг<br />
			Центр - санаторий Алатау 1000 тг<br />
			(экологический пост оплачивает клиент)
		</div>
		<div class="item3" style="top: 445px; left: 387px;">
			г. Капчагай 4000 тг<br />
			зоны отдыха Капчагая 5000 тг<br />
			г. Каскелен 2000 тг<br />
			Узун-Агач 4000 тг<br />
			Боролдай 1500 тг<br />
			г. Талгар 2500 тг<br />
			г. Иссык 3800 тг<br />
			Туздыбастау 1500 тг<br />
			Белбулак 2000 тг<br />
			Тургень 4500 тг<br />
			Чунджа 12000 тг
		</div>

		<div class="item4" style="top: 112px; left: 732px;"><img src="<?php echo base_url()?>media/design/tariffs_block1_item4_1.png"></div>
		<div class="item4" style="top: 265px; left: 732px;"><img src="<?php echo base_url()?>media/design/tariffs_block1_item4_2.png"></div>
		<div class="item4" style="top: 398px; left: 732px;"><img src="<?php echo base_url()?>media/design/tariffs_block1_item4_3.png"></div>
		<div class="item4" style="top: 644px; left: 732px;"><img src="<?php echo base_url()?>media/design/tariffs_block1_item4_4.png"></div>
		<div class="item4" style="top: 815px; left: 732px;"><img src="<?php echo base_url()?>media/design/tariffs_block1_item4_5.png"></div>

		<div class="item5" style="top: 942px; left: 397px;"><img src="<?php echo base_url()?>media/design/tariffs_block1_item5.png"></div>

		<div class="item6" style="top: 955px; left: 568px;">
			Все тарифы уточняйте<br />
			у диспетчера по телефонам:
		</div>
		<div class="item7" style="top: 1002px; left: 568px;">
			+7 (727) 338-77-77<br />
			+7 (702) 221-30-30
		</div>
	</div></div>
</div>